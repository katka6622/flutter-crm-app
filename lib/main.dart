import 'package:flutter/material.dart';
import 'package:flutter_crm_app/screens/employee/employeeWorkingDaysTab.dart';
import 'package:flutter_crm_app/screens/finance/dashboard.dart';
import 'package:flutter_crm_app/screens/finance/financeListTab.dart';
import 'package:flutter_crm_app/screens/products/pickPageTab.dart';
import 'package:flutter_crm_app/screens/setting/SettingsTab.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:flutter_crm_app/screens/employee/employeeListTab.dart';
import 'package:flutter_crm_app/screens/record/recordListTab.dart';
import 'package:flutter_crm_app/screens/services/serviceListTab.dart';
import 'package:flutter_crm_app/screens/clients/usersListTab.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/screens/splash_screen.dart';
import 'package:flutter_crm_app/screens/products/productsListTab.dart';
import 'package:flutter_crm_app/screens/products/trafficProductTab.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main()  => runApp(MyApp());

DatabaseHelper helper = DatabaseHelper.instance;

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final routes = <String, WidgetBuilder>{
    // Путь, по которому создаётся Home Screen
    "/clients": (_) => new UsersListTab(),
    "/services": (_) => new ServiceListTab(),
    "/employees": (_) => new EmployeeListTab(),
    "/products": (_) => new PickPageTab(),
    "/records": (_) => new RecordListTab(),
    "/trafficProduct": (_) => new TrafficProductTab(),
    "/finance": (_) => new FinanceListTab(),
    "/workingDays": (_) => new EmployeeWorkingDaysTab(),
    "/settings": (_) => new SettingsTab(),
    "/dashboard": (_) => new DashboardTab(),
  };
  @override
  Widget build(BuildContext context) {
    helper.initDB();
    // Это будет приложение с поддержкой Material Design
    return MaterialApp(
      title: 'Admin Assistant',
      theme: ThemeData(primaryColor: Color.fromRGBO(252, 245, 184, 1)),
      // в котором будет Splash Screen с указанием следующего маршрута
      home: SplashScreen(nextRoute: '/records'),
      // передаём маршруты в приложение
      routes: routes,
      // locale: Locale.fromSubtags(languageCode: 'RU'),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
          const Locale('en', 'US'),
          const Locale('ru', 'RU'),
      ],
    );
  }
}
