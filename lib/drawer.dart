import 'package:flutter/material.dart';
import 'package:flutter_crm_app/screens/employee/employeeListTab.dart';
import 'package:flutter_crm_app/screens/employee/employeeWorkingDaysTab.dart';
import 'package:flutter_crm_app/screens/finance/dashboard.dart';
import 'package:flutter_crm_app/screens/products/pickPageTab.dart';
import 'package:flutter_crm_app/screens/finance/financeListTab.dart';
import 'package:flutter_crm_app/screens/record/recordListTab.dart';
import 'package:flutter_crm_app/screens/services/serviceListTab.dart';
import 'package:flutter_crm_app/screens/clients/usersListTab.dart';
import 'package:flutter_crm_app/screens/setting/SettingsTab.dart';

class AndroidDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        // color: Color.fromRGBO(252, 245, 184, 1),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
                width: double.infinity,
                height: 96.8,
                child: DrawerHeader(
                  decoration:
                      BoxDecoration(color: Color.fromRGBO(68, 192, 192, 0.8)),
                  // image: DecorationImage(
                  //     image: AssetImage("images/logo.png"))
                  // margin: const EdgeInsets.only(bottom: 0),
                  child: Stack(
                    children: <Widget>[
                      Text(
                        'admin assistant',
                        style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          foreground: Paint()
                            ..strokeWidth = 2
                            ..style = PaintingStyle.stroke
                            ..color = Colors.black,
                        ),
                      ),
                      Text(
                        'admin assistant',
                        style: TextStyle(
                          fontSize: 18.0,
                          color: Color.fromRGBO(252, 245, 184, 1),
                        ),
                      )
                    ],
                  ),
                )),
            Column(
              children: <Widget>[
                ListTile(
                  leading: RecordListTab.androidIcon,
                  title: Text(RecordListTab.title),
                  onTap: () {
                    Navigator.pushReplacementNamed(context, "/records");
                  },
                ),
                _divider(),
                ListTile(
                  leading: EmployeeWorkingDaysTab.androidIcon,
                  title: Text(EmployeeWorkingDaysTab.title),
                  onTap: () {
                    Navigator.pushReplacementNamed(context, "/workingDays");
                  },
                ),
                _divider(),
                ListTile(
                  leading: FinanceListTab.androidIcon,
                  title: Text(FinanceListTab.title),
                  onTap: () {
                    Navigator.pushReplacementNamed(context, "/finance");
                  },
                ),
                _divider(),
                ListTile(
                  leading: UsersListTab.androidIcon,
                  title: Text(UsersListTab.title),
                  onTap: () {
                    Navigator.pushReplacementNamed(context, "/clients");
                  },
                ),
                _divider(),
                ListTile(
                  leading: ServiceListTab.androidIcon,
                  title: Text(ServiceListTab.title),
                  onTap: () {
                    Navigator.pushReplacementNamed(context, "/services");
                  },
                ),
                _divider(),
                ListTile(
                  leading: EmployeeListTab.androidIcon,
                  title: Text(EmployeeListTab.title),
                  onTap: () {
                    Navigator.pushReplacementNamed(context, "/employees");
                  },
                ),
                _divider(),
                ListTile(
                  leading: PickPageTab.androidIcon,
                  title: Text(PickPageTab.title),
                  onTap: () {
                    Navigator.pushReplacementNamed(context, "/products");
                  },
                ),
                _divider(),
                 ListTile(
                  leading: SettingsTab.androidIcon,
                  title: Text(SettingsTab.title),
                  onTap: () {
                    Navigator.pushReplacementNamed(context, "/settings");
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  _divider() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
      child: Divider(),
    );
  }
}
