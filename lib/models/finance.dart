import 'baseModel.dart';

class Finance extends BaseModel {  
  static const String income = 'доход';
  static const String outcome = 'расход';
  static const List<String> consumptionList = [
    'Доминикана для Катьки',
    'Заказ воды',
    'Заказ еды',
    'Ремонт оборудования',
    'Корпоратив',
  ];

  int id;
  String financeType;
  DateTime dateCreate;
  int money = 0;
  String comment = '';
  String nameWhoPay;
  int clientId;
  int masterId;
  int recordId;

  Finance(
      {this.id,
      this.financeType,
      this.dateCreate,
      this.money,
      this.comment,
      this.nameWhoPay,
      this.clientId,
      this.masterId,
      this.recordId});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'financeType': financeType,
      'dateCreate': DateTime.now().toIso8601String(),
      'money': money ?? 0,
      'comment': comment ?? '',
      'nameWhoPay': nameWhoPay,
      'clientId': clientId,
      'masterId': masterId,
      'recordId': recordId
    };
  }

  @override
  String toString() {
    return 'Finance{id: $id, financeType: $financeType, money: $money, comment: $comment, nameWhoPay: $nameWhoPay, clientId: $clientId, masterId: $masterId, recordId: $recordId}';
  }
}
