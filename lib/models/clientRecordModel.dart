import 'package:flutter_crm_app/models/clientRecord.dart';

class ClientRecordModel extends ClientRecord {
  int id;
  int clientId;
  int serviceId;
  int masterId;
  DateTime recordDate;
  int recordDuration;
  String recordStatus;
  int serviceCost;
  String clientName;
  String clientPhone;
  String masterName;
  String serviceName;

  static const visitStatusList = ['запись', 'завершен'];

  ClientRecordModel(
      {this.id,
      this.clientId,
      this.serviceId,
      this.masterId,
      this.recordDate,
      this.recordDuration,
      this.recordStatus,
      this.serviceCost,
      this.masterName,
      this.clientName,
      this.clientPhone,
      this.serviceName})
      : super(
          id: id,
          clientId: clientId,
          masterId: masterId,
          recordDate: recordDate,
          recordStatus: recordStatus,
          recordDuration: recordDuration,
        );

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'clientId': clientId,
      'serviceId': serviceId,
      'masterId': masterId,
      'recordDate': recordDate.toIso8601String(),
      'recordDuration': recordDuration,
      'recordStatus': recordStatus,
      'serviceCost': serviceCost,
      'clientName': clientName,
      'clientPhone': clientPhone,
      'masterName': masterName,
      'serviceName': serviceName,
    };
  }

  @override
  String toString() {
    final recordDateString = this.recordDate.toIso8601String();
    return 'ClientRecordModel{id: $id, clientId: $clientId, serviceId: $serviceId, masterId: $masterId, recordDate: $recordDateString, recordDuration: $recordDuration}';
  }
}
