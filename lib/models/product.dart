import 'baseModel.dart';

class Product extends BaseModel {
  int id;
  String name;
  int quantity;
  int costSelf;
  int costSell;
  int costwriteOff;
  int writeOffCount;
  int writeOffCapacity;

  Product({
    this.id,
    this.name,
    this.quantity = 0,
    this.costSelf = 0,
    this.costSell = 0,
    this.costwriteOff = 0,
    this.writeOffCount = 0,
    this.writeOffCapacity = 0,
  });
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'quantity': quantity ?? 0,
      'costSelf': costSelf ?? 0,
      'costSell': costSell ?? 0,
      'costWriteOff': costwriteOff ?? 0,
      'writeOffCount': writeOffCount ?? 0,
      'writeOffCapacity': writeOffCapacity ?? 0,
    };
  }

  @override
  String toString() {
    return 'Product{id: $id, name: $name, quantity: $quantity, costSelf: $costSelf, costSell: $costSell, costWriteOff: $costwriteOff, writeOffCount: $writeOffCount, writeOffCapacity: $writeOffCapacity}';
  }

  void add(value) {}
}
