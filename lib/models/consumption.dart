import 'baseModel.dart';

class ConsumptionType extends BaseModel {
  int id;
  String name;

  ConsumptionType({
    this.id,
    this.name,
  });
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }
   @override
  String toString() {
    return 'ConsumptionType{id: $id, name: $name}';
  }
}