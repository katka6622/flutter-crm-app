import 'baseModel.dart';

class Client extends BaseModel {
  int id;
  String name;
  String phone;
  int recordCount = 0;
  DateTime lastRecord;
  String comment = '';
  int moneySpend = 0;
  DateTime birthday;
  String gender;

  Client({
    this.id,
    this.name,
    this.phone,
    this.recordCount,
    this.lastRecord,
    this.comment,
    this.moneySpend,
    this.birthday,
    this.gender,
  });


  Map<String, dynamic> toMap() {
    var lastVisitValue = lastRecord != null ? lastRecord.toIso8601String() : null;
    var birthdayValue = birthday != null ? birthday.toIso8601String() : null;
    return {
      'id': id,
      'name': name,
      'phone': phone,
      'recordCount': recordCount ?? 0,
      'lastRecord': lastVisitValue,
      'comment': comment ?? '',
      'moneySpend': moneySpend ?? 0,
      'birthday': birthdayValue,
      'gender': gender,
    };
  }

  @override
  String toString() {
    final lastVisitString =
        this.lastRecord != null ? this.lastRecord.toIso8601String() : null;
    final birthdayValue = this.birthday != null ? this.birthday.toIso8601String() : null;
    return 'Client{id: $id, name: $name, phone: $phone, recordCount: $recordCount, lastRecord: $lastVisitString, comment: $comment, moneySpend: $moneySpend, birthday: $birthdayValue, gender: $gender}';
  }
}
