import 'baseModel.dart';

class EmployeeWorkDay extends BaseModel {
  int id;
  DateTime date;
  int isWorkDay;
  int employeeId;

  EmployeeWorkDay(
      {this.id,
      this.date,
      this.isWorkDay,
      this.employeeId
      });

  Map<String, dynamic> toMap() {
    var dateValue = date != null ? date.toIso8601String() : null;
    return {
      'id': id,
      'date': dateValue,
      'isWorkDay': isWorkDay,
      'employeeId': employeeId,
    };
  }

  @override
  String toString() {
    final date = this.date != null ? this.date.toIso8601String() : null;
    return 'EmployeeWorkDay{id: $id, date: $date, isWorkDay: $isWorkDay, employeId: $employeeId}';
  }
}
