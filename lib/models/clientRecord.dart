import 'baseModel.dart';

class ClientRecord extends BaseModel {
  int id;
  int clientId;
  int masterId;
  DateTime recordDate;
  int recordDuration;
  String recordStatus;
  int serviceCost;

  static const String paidStatus = 'оплачен';
  static const String nonPaidStatus = 'не оплачен';

  static const visitStatusList = ['запись', 'завершен'];

  ClientRecord(
      {this.id,
      this.clientId,
      this.masterId,
      this.recordDate,
      this.recordDuration = 0,
      this.recordStatus = 'не оплачен',
      this.serviceCost});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'clientId': clientId,
      'masterId': masterId,
      'recordDate': recordDate.toIso8601String(),
      'recordDuration': recordDuration,
      'recordStatus': recordStatus,
      'serviceCost': serviceCost,
    };
  }

  @override
  String toString() {
    final recordDateString = this.recordDate != null ? this.recordDate.toIso8601String() : 'NULL';
    return 'ClientRecord{id: $id, clientId: $clientId, masterId: $masterId, recordDate: $recordDateString, recordDuration: $recordDuration, recordStatus: $recordStatus}';
  }
}
