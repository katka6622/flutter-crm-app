import 'baseModel.dart';

class Employee extends BaseModel {
  int id;
  String name;
  String phone;
  String role; //admin, master, owner
  String speciality;
  DateTime birthday;
  String gender;
  int salaryValue;
  num salaryPercent;

  Employee(
      {this.id,
      this.name,
      this.phone,
      this.role,
      this.speciality = '',
      this.birthday,
      this.gender,
      this.salaryValue = 0,
      this.salaryPercent = 0
      });

  Map<String, dynamic> toMap() {
    var birthdayValue = birthday != null ? birthday.toIso8601String() : null;
    return {
      'id': id,
      'name': name,
      'phone': phone,
      'role': role,
      'speciality': speciality,
      'birthday': birthdayValue,
      'gender': gender,
      'salaryValue': salaryValue,
      'salaryPercent': salaryPercent,
    };
  }

  @override
  String toString() {
    final birthdayValue = this.birthday != null ? this.birthday.toIso8601String() : null;
    return 'Employee{id: $id, name: $name, phone: $phone, role: $role, speciality: $speciality, birthday: $birthdayValue, gender: $gender, salaryPercent: $salaryPercent, salaryValue: $salaryValue}';
  }
}
