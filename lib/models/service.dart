import 'baseModel.dart';

class Service extends BaseModel{
  int id;
  String name;
  int cost = 0;
  int duration = 0;
  String comment = '';

  Service(
      {this.id,
      this.name,
      this.cost,
      this.duration,
      this.comment});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'duration': duration ?? 0,
      'cost': cost ?? 0,
      'comment': comment ?? ''
    };
  }

  @override
  String toString() {
    return 'Service{id: $id, name: $name, cost: $cost, duration: $duration, comment: $comment}';
  }
}
