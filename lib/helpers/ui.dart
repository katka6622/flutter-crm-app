import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_crm_app/models/baseModel.dart';
import 'package:flutter_crm_app/models/clientRecordModel.dart';
import 'package:flutter_crm_app/models/service.dart';
import 'package:flutter_week_view/flutter_week_view.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';

class ConstantValues {
  static const gengerList = <String>['муж', 'жен'];
  static const roleList = <String>['Мастер', 'Администратор', 'Директор'];
}

Widget launchRaisedButton(String text, double fontSizeValue, String launchUri) {
  return RaisedButton(
      onPressed: () => launch(launchUri),
      color: Color.fromRGBO(68, 192, 192, 1),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5))),
      child: new Text(
        text,
        style: TextStyle(fontSize: fontSizeValue),
      ));
}

Widget launchRaisButton(String text, String launchUri, IconData icon) {
  return SizedBox(
    width: 125.0,
    child: RaisedButton(
      onPressed: () => launch(launchUri),
      color: Color.fromRGBO(252, 245, 184, 1),
      shape: RoundedRectangleBorder(
          side: BorderSide(color: Color.fromRGBO(68, 192, 192, 1)),
          borderRadius: BorderRadius.all(Radius.circular(5.0))),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
        Icon(icon, color: Color.fromRGBO(68, 192, 192, 1), size: 12.0),
        Text(text),
      ]),
    ),
  );
}

String formatPercent(value) {
  return value + '%';
}

String formatTime(int duration) {
  int hours = duration ~/ 60;
  int minutes = duration % 60;

  if (hours == 0) {
    return minutes.toString() + ' мин.';
  }
  if (minutes == 0) {
    return hours.toString() + 'ч.';
  }
  return hours.toString() + 'ч. ' + minutes.toString() + 'мин.';
}

String formatTimeOutput(DateTime time) {
  String hour =
      time.hour < 10 ? '0' + time.hour.toString() : time.hour.toString();
  String minute =
      time.minute < 10 ? '0' + time.minute.toString() : time.minute.toString();
  return hour + ':' + minute;
}

String formatDateOutput(DateTime time) {
  String month =
      time.month < 10 ? '0' + time.month.toString() : time.month.toString();
  String day = time.day < 10 ? '0' + time.day.toString() : time.day.toString();
  return day + '-' + month + '-' + time.year.toString();
}

String formatDateTime(DateTime dateTime) {
  return formatDateOutput(dateTime) + ' ' + formatTimeOutput(dateTime);
}

String formatPhone(String phone) {
  var phoneValue = phone.startsWith('8') ? phone.replaceFirst('8', '7') : phone;
  var controller =
      new MaskedTextController(text: '', mask: '+0 (000) 000-00-00');
  controller.updateText(phoneValue);
  return controller.text;
}

String formatMoney(int money) {
  String leftSymbol = money < 0 ? '-' : '';
  var moneyController = MoneyMaskedTextController(
      rightSymbol: ' ₽',
      leftSymbol: leftSymbol,
      initialValue: money.toDouble(),
      thousandSeparator: ' ',
      decimalSeparator: '',
      precision: 0);
  return moneyController.text;
}

Widget commonFormPadding({double padding}) {
  double paddingValue = padding ?? 10.0;
  return Padding(padding: EdgeInsets.symmetric(vertical: paddingValue));
}

Widget dropdownField(items, callback, currentValue) {
  return DropdownButton<String>(
    key: new Key('dropdown' + DateTime.now().toString()),
    iconEnabledColor: Color.fromRGBO(68, 192, 192, 1),
    iconDisabledColor: Color.fromRGBO(68, 192, 192, 1),
    value: currentValue,
    icon: Icon(Icons.arrow_drop_down),
    iconSize: 24,
    elevation: 16,
    underline: Container(
      height: 2,
      color: Color.fromRGBO(68, 192, 192, 1),
    ),
    onChanged: callback,
    items: items.map<DropdownMenuItem<String>>((String value) {
      return DropdownMenuItem<String>(
        value: value,
        child: Text(value),
      );
    }).toList(),
  );
}

Widget phoneFormField(String phone, callback) {
  var controller =
      new MaskedTextController(text: phone, mask: '0 (000) 000-00-00');
  return Container(
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
        Text('Телефон:', style: TextStyle(fontWeight: FontWeight.bold)),
        TextFormField(
            controller: controller,
            keyboardType: TextInputType.phone,
            decoration: const InputDecoration(
              hintText: 'Введите телефон клиента',
            ),
            validator: (phone) {
              if (phone.length != 17) {
                return 'Введите корректный номер телефона';
              }
              return null;
            },
            onSaved: callback),
      ]));
}

Widget timepicker(callback, {DateTime initialTime}) {
  initialTime =
      initialTime == null ? DateTime(2000, 01, 01, 01, 00) : initialTime;
  return new TimePickerSpinner(
    is24HourMode: true,
    time: initialTime,
    minutesInterval: 30,
    normalTextStyle:
        TextStyle(fontSize: 18, color: Color.fromRGBO(68, 192, 192, 1)),
    highlightedTextStyle: TextStyle(fontSize: 18),
    spacing: 50,
    itemHeight: 80,
    isForce2Digits: true,
    onTimeChange: callback,
  );
}

Widget popupTimePickerField(
    context, controller, callback, Widget upperTextWidget, String saveText,
    {DateTime initialTime}) {
  return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        upperTextWidget,
        TextFormField(
          readOnly: true,
          controller: controller,
          onTap: () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                      content: timepicker(callback, initialTime: initialTime),
                      actions: [
                        FlatButton(
                          child: Text(saveText),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ]);
                });
          },
        )
      ]);
}

Widget masterRecord(double recordHeight, ClientRecordModel clientRecordModel,
    DateTime beginTime, DateTime endTime) {
  return Container(
    padding: EdgeInsets.all(0),
    margin: EdgeInsets.all(0),
    height: recordHeight,
    width: 200,
    decoration: BoxDecoration(
      color: Colors.green[300],
      border: Border.all(width: 0.8),
      borderRadius: BorderRadius.circular(5),
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(formatTimeOutput(beginTime) + ' - ' + formatTimeOutput(endTime)),
        Text(clientRecordModel.clientName),
        Text(formatPhone(clientRecordModel.clientPhone)),
        Text(clientRecordModel.serviceName),
      ],
    ),
  );
}

Widget buildTopMastersGrid(masterName) {
  return Container(
    height: 70.0,
    width: 200,
    padding: const EdgeInsets.symmetric(vertical: 20),
    child: Center(child: Text(masterName.toString())),
    color: Color.fromRGBO(68, 192, 192, 1),
  );
}

Widget leftOneHourContainer(
    double width, double hourHeight, index, _startTime) {
  var _timeFrame = _startTime.add(HourMinute(hour: index));
  var _visibleHour = _timeFrame.hour < 10
      ? '0' + _timeFrame.hour.toString()
      : _timeFrame.hour.toString();

  return Container(
    height: hourHeight,
    width: width,
    padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
    margin: EdgeInsets.all(0),
    child: Text(_visibleHour + ':00'),
    color: Color.fromRGBO(68, 192, 192, 1),
  );
}

Widget searchPopupField(
    futureList, hintText, searchHintText, selectedValue, callback, validator) {
  return new FutureBuilder(
    future: futureList,
    builder: (context, snapshot) {
      List masters = _convertToDropdown(snapshot.data) ?? [];
      return SearchableDropdown.single(
        items: masters,
        value: selectedValue,
        hint: hintText,
        searchHint: searchHintText,
        searchFn: searchClientFn,
        onChanged: callback,
        validator: validator,
        isExpanded: true,
        closeButton: 'Закрыть',
      );
    },
  );
}

Widget stubPopup(items, selectedValue, hintText, searchText, callback) {
  List<DropdownMenuItem<String>> result = [];
  if (items != null) {
    items.forEach((element) {
      DropdownMenuItem<String> newElem =
          new DropdownMenuItem(value: element, child: Text(element));
      result.add(newElem);
    });
  }
  return SearchableDropdown.single(
        items: result,
        value: selectedValue,
        hint: hintText,
        searchHint: searchText,
        onChanged: callback,
        isExpanded: true,
        closeButton: 'Закрыть',
      );
}

List<int> searchClientFn(keyword, List items) {
  List<int> res = [];
  if (items != null) {
    items.asMap().forEach((index, element) {
      if (element.value.name.toLowerCase().contains(keyword) ||
          element.value.phone.contains(keyword)) {
        res.add(index);
      }
    });
  }

  return res;
}

List<DropdownMenuItem<BaseModel>> _convertToDropdown(List dataList) {
  List<DropdownMenuItem<BaseModel>> result = [];
  if (dataList != null) {
    dataList.forEach((element) {
      DropdownMenuItem<BaseModel> newElem =
          new DropdownMenuItem(value: element, child: Text(element.name));
      result.add(newElem);
    });
  }
  return result;
}
