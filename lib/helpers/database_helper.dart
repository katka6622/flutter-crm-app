import 'package:flutter_crm_app/models/clientRecordModel.dart';
import 'package:flutter_crm_app/models/consumption.dart';
import 'package:flutter_crm_app/models/employee.dart';
import 'package:flutter_crm_app/models/employeeWorkDay.dart';
import 'package:flutter_crm_app/models/finance.dart';
import 'package:flutter_crm_app/models/product.dart';
import 'package:flutter_crm_app/models/service.dart';
import 'package:flutter_date_pickers/flutter_date_pickers.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:math';
// import 'package:path_provider/path_provider.dart';

import 'package:flutter_crm_app/models/client.dart';
import 'package:flutter_crm_app/models/clientRecord.dart';

const initScripts = [
  """CREATE TABLE clients(
        id INTEGER PRIMARY KEY, 
        name TEXT, 
        phone TEXT NOT NULL,
        recordCount INTEGER,
        lastRecord DATETIME,
        comment TEXT,
        moneySpend INTEGER,
        birthday DATETIME,
        gender TEXT
        );""",
  """CREATE TABLE services(
        id INTEGER PRIMARY KEY, 
        name TEXT NOT NULL, 
        cost INTEGER NOT NULL,
        duration INTEGER NOT NULL,
        comment TEXT NOT NULL
        );""",
  """CREATE TABLE employee(
        id INTEGER PRIMARY KEY, 
        name TEXT NOT NULL, 
        phone TEXT NOT NULL,
        role TEXT NOT NULL,
        speciality TEXT NOT NULL,
        birthday DATETIME,
        gender TEXT,
        salaryValue INT,
        salaryPercent REAL
        );""",
  """CREATE TABLE product(
        id INTEGER PRIMARY KEY, 
        name TEXT NOT NULL,
        quantity INT,
        writeOffCount INT,
        writeOffCapacity INT,
        costSelf INT,
        costSell INT,
        costWriteOff INT
        );""",
  """CREATE TABLE clientRecord(
        id INTEGER PRIMARY KEY, 
        clientId INTEGER NOT NULL,
        masterId INTEGER NOT NULL,
        recordDate DATETIME NOT NULL,
        recordDuration INT NOT NULL,
        recordStatus TEXT,
        serviceCost INT,
        FOREIGN KEY (clientId) REFERENCES clients (id) 
                ON DELETE NO ACTION ON UPDATE NO ACTION,
        FOREIGN KEY (masterId) REFERENCES employee (id) 
                ON DELETE NO ACTION ON UPDATE NO ACTION  
        );""",
  """CREATE TABLE recordServices(
        id INTEGER PRIMARY KEY, 
        serviceId INTEGER NOT NULL,
        recordId INTEGER NOT NULL
        );""",
  """CREATE TABLE finance(
        id INTEGER PRIMARY KEY, 
        financeType TEXT,
        dateCreate DATETIME NOT NULL,
        money INTEGER NOT NULL,
        comment TEXT,
        nameWhoPay TEXT,
        recordId INTEGER,
        clientId INTEGER,
        masterId INTEGER
        );""",
  """CREATE TABLE consumptionType(
        id INTEGER PRIMARY KEY, 
        name TEXT
        );""",
  """CREATE TABLE employeeWorkDays(
        id INTEGER PRIMARY KEY, 
        employeeId INT NOT NULL,
        date DATETIME NOT NULL,
        isWorkDay INT
        );""",
];

// singleton class to manage the database
class DatabaseHelper {
  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "MyDatabase47.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 1;

  // Make this a singleton class.
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();

    return _database;
  }

  // open the database
  _initDatabase() async {
    print('init DB');
    String path = join(await getDatabasesPath(), _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    print('create DB');
    var batch = db.batch();
    initScripts.forEach((script) => {batch.execute(script)});
    batch.commit();

    //TODO remove initial data insertion
    await new Future.delayed(const Duration(seconds: 1));
    // _fillDB(db);
  }

  Future _clearDB() async {
    await _database.execute('DELETE FROM clients;');
    await _database.execute('DELETE FROM clientRecord;');
    await _database.execute('DELETE FROM services;');
    await _database.execute('DELETE FROM employee;');
    await _database.execute('DELETE FROM product;');
    await _database.execute('DELETE FROM consumptionType;');
    await _database.execute('DELETE FROM finance;');
    await _database.execute('DELETE FROM recordServices;');
  }

  Future _fillDB(Database db) async {
    await db;
    await new Future.delayed(const Duration(seconds: 2));
    insertClient(Client(
      name: 'Alex 1',
      phone: '89822454601',
      recordCount: 1,
      comment: 'красавчик',
      lastRecord: DateTime(2020, 01, 15, 19, 30, 00),
      moneySpend: 3400,
      gender: 'муж',
    ));

    insertClient(Client(
      name: 'Alex 2',
      phone: '89822454602',
      recordCount: 2,
      comment: 'тоже красавчик',
      lastRecord: DateTime(2020, 03, 10, 16, 30, 00),
      moneySpend: 2500,
      gender: 'муж',
    ));

    insertClient(Client(
      name: 'Kat 1',
      phone: '89822454603',
      recordCount: 12,
      comment: 'шикарная',
      lastRecord: DateTime(2010, 03, 10, 16, 30, 00),
      moneySpend: 12500,
      gender: 'жен',
      birthday: DateTime(1994, 10, 18),
    ));

    insertService(Service(
      name: 'Подпилить ноготочек',
      cost: 80,
      duration: 60,
      comment: 'Только для однопалых столяров',
    ));

    insertService(Service(
      name: 'Подпилить все ноготочки',
      cost: 900,
      duration: 80,
      comment: 'Чисто по красоте',
    ));

    insertService(Service(
      name: 'Мужской маникюр',
      cost: 1200,
      duration: 60,
      comment: 'БРУТАЛЬНО',
    ));

    insertService(Service(
      name: 'Маникюр с покрытием',
      cost: 1600,
      duration: 120,
      comment:
          'одинарная база, двойной топ, тройной цвет, квадратичный узор, гексагональная цена',
    ));

    insertEmployee(Employee(
      name: 'Кетрин',
      phone: '79026484664',
      role: 'Администратор',
      speciality: 'Маникюрных дел мастер и просто красавица',
      birthday: DateTime(1994, 10, 18),
      gender: 'жен',
    ));
    insertEmployee(Employee(
      name: 'Мастер Алекс',
      phone: '79822454601',
      role: 'Мастер',
      speciality: 'Мастер приложения',
      birthday: DateTime(1993, 01, 29),
      gender: 'муж',
    ));
    insertEmployee(Employee(
      name: 'Борис Бритва',
      phone: '79822454601',
      role: 'Мастер',
      speciality: 'Мастер бритвы',
      birthday: DateTime(1993, 01, 29),
      gender: 'муж',
    ));
    insertProduct(Product(
      name: 'шампунька',
      costSelf: 200,
      costSell: 500,
      quantity: 10,
      writeOffCapacity: 50,
      writeOffCount: 20,
    ));
    insertProduct(Product(
      name: 'мягкая шампунька',
      costSelf: 400,
      costSell: 850,
      quantity: 5,
      writeOffCapacity: 200,
      writeOffCount: 100,
    ));
    insertProduct(Product(
      name: 'мега мягкая шампунька',
      costSelf: 500,
      costSell: 1000,
      quantity: 3,
      writeOffCapacity: 100,
      writeOffCount: 0,
    ));
    // generateRecordEvents();
    print("filled DB");
  }

  // BEGIN CLIENTS
  Future<int> insertClient(Client client) async {
    return await _database.insert('clients', client.toMap());
  }

  Future<int> updateClient(Client client) async {
    return await _database.update('clients', client.toMap(),
        where: 'id = ?', whereArgs: [client.id]);
  }

  void initDB() async {
    await database;
  }

  Future<int> removeClient(int id) async {
    return await _database.delete('clients', where: 'id = ?', whereArgs: [id]);
  }

  Future<Client> getClient(int id) async {
    final List<Map<String, dynamic>> maps =
        await _database.query('clients', where: 'id = ?', whereArgs: [id]);
    return Client(
      id: maps[0]['id'],
      name: maps[0]['name'],
      phone: maps[0]['phone'],
    );
  }

  Future<List<Client>> clients() async {
    // await _clearDB();
    // await _fillDB(_database);
    final List<Map<String, dynamic>> maps =
        await _database.query('clients', orderBy: 'name ASC');
    return List.generate(maps.length, (i) {
      return Client(
        id: maps[i]['id'],
        name: maps[i]['name'],
        phone: maps[i]['phone'],
        recordCount: maps[i]['recordCount'],
        lastRecord: maps[i]['lastRecord'] != null
            ? DateTime.parse(maps[i]['lastRecord'])
            : null,
        comment: maps[i]['comment'],
        moneySpend: maps[i]['moneySpend'],
        birthday: maps[i]['birthday'] != null
            ? DateTime.parse(maps[i]['birthday'])
            : null,
      );
    });
  }

  Future<List<Client>> findClients(String searchString) async {
    final List<Map<String, dynamic>> results = await _database
        .rawQuery('''SELECT clients.id, clients.name, clients.phone
        FROM clients WHERE name LIKE ? OR phone LIKE ?
        ''', [searchString + '%', '%' + searchString + '%']);
    return List.generate(results.length, (i) {
      return Client(
        id: results[i]['id'],
        name: results[i]['name'],
        phone: results[i]['phone'],
      );
    });
  }
  // END CLIENTS

  /* BEGIN CLIENT RECORDS */
  Future<List<ClientRecordModel>> clientRecordList(dateFrom, dateTo) async {
    final List<Map<String, dynamic>> maps = await _database.rawQuery(
        '''SELECT clientRecord.*, clients.name as clientName, clients.phone as clientPhone, employee.name as masterName, GROUP_CONCAT(services.name, ', ') as serviceName 
        FROM clientRecord
        INNER JOIN clients ON clients.id = clientRecord.clientId
        INNER JOIN employee ON employee.id = clientRecord.masterId
        INNER JOIN recordServices ON recordServices.recordId = clientRecord.id
        INNER JOIN services ON services.id = recordServices.serviceId
        WHERE recordDate BETWEEN ? AND ?
        GROUP BY
        clientRecord.id
        ''', [dateFrom, dateTo]);
    return List.generate(maps.length, (i) {
      return ClientRecordModel(
        id: maps[i]['id'],
        clientId: maps[i]['clientId'],
        serviceId: maps[i]['serviceId'],
        masterId: maps[i]['masterId'],
        recordDate: DateTime.parse(maps[i]['recordDate']),
        recordDuration: maps[i]['recordDuration'],
        recordStatus: maps[i]['recordStatus'],
        clientName: maps[i]['clientName'],
        clientPhone: maps[i]['clientPhone'],
        masterName: maps[i]['masterName'],
        serviceName: maps[i]['serviceName'],
      );
    });
  }

  Future<ClientRecordModel> getClientRecordInfoById(
      ClientRecord clientRecord) async {
    final List<Map<String, dynamic>> maps = await _database.rawQuery(
        '''SELECT clientRecord.*, clients.name as clientName, clients.phone as clientPhone, employee.name as masterName, GROUP_CONCAT(services.name, ', ') as serviceName 
        FROM clientRecord
        INNER JOIN clients ON clients.id = clientRecord.clientId
        INNER JOIN employee ON employee.id = clientRecord.masterId
        INNER JOIN recordServices ON recordServices.recordId = clientRecord.id
        INNER JOIN services ON services.id = recordServices.serviceId
        WHERE clientRecord.id = ?
        GROUP BY
        clientRecord.id
        ''', [clientRecord.id]);
    var clientRecordModel = ClientRecordModel(
      id: maps[0]['id'],
      clientId: maps[0]['clientId'],
      serviceId: maps[0]['serviceId'],
      masterId: maps[0]['masterId'],
      recordDate: DateTime.parse(maps[0]['recordDate']),
      recordDuration: maps[0]['recordDuration'],
      recordStatus: maps[0]['recordStatus'],
      clientName: maps[0]['clientName'],
      clientPhone: maps[0]['clientPhone'],
      masterName: maps[0]['masterName'],
      serviceName: maps[0]['serviceName'],
    );
    return clientRecordModel;
  }

  Future<List<int>> getClientRecordServiceIds(ClientRecord clientRecord) async {
    final List<Map<String, dynamic>> maps =
        await _database.rawQuery('''SELECT services.id as serviceId 
        FROM clientRecord
        INNER JOIN recordServices ON recordServices.recordId = clientRecord.id
        INNER JOIN services ON services.id = recordServices.serviceId
        WHERE clientRecord.id = ?
        ''', [clientRecord.id]);
    List<int> result = [];
    print(maps);
    maps.forEach((element) {
      result.add(element['serviceId'] - 1);
    });
    print(result);
    return result;
  }

  Future<int> clientRecordCount(int clientId) async {
    return Sqflite.firstIntValue(await _database.rawQuery(
        'SELECT COUNT(*) FROM clientRecord WHERE clientId = ?', [clientId]));
  }

  Future<int> clientRecordMoneySpend(int clientId) async {
    var moneySpend = Sqflite.firstIntValue(await _database.rawQuery(
        'SELECT SUM(serviceCost) FROM clientRecord WHERE clientId = ?',
        [clientId]));
    return moneySpend != null ? moneySpend : 0;
  }

  Future<int> clientRecordAverageCost(int clientId) async {
    var averageCheck = Sqflite.firstIntValue(await _database.rawQuery(
        'SELECT CAST(AVG(serviceCost) as INTEGER) FROM clientRecord WHERE clientId = ?',
        [clientId]));
    return averageCheck != null ? averageCheck : 0;
  }

  Future updateClientRecordStatus(
      ClientRecord clientRecord, String newStatus) async {
    await _database.rawQuery(
      'UPDATE clientRecord SET recordStatus = ? WHERE id = ? ',
      [newStatus, clientRecord.id],
    );
  }

  Future<Map> updateClientRecordDateTime(
      ClientRecordModel clientRecordModel) async {
    var dateEnd = clientRecordModel.recordDate
        .add(new Duration(minutes: clientRecordModel.recordDuration));
    List<Map<String, dynamic>> allow = await _database.rawQuery(
      'SELECT COUNT(1) as count FROM clientRecord WHERE masterId = ? AND recordDate >= ? AND recordDate <= ?',
      [
        clientRecordModel.masterId,
        clientRecordModel.recordDate.toIso8601String(),
        dateEnd.toIso8601String()
      ],
    );

    if (allow[0]['count'] > 0) {
      return {'result': 'error', 'id': 0};
    }
    await _database.rawQuery(
      'UPDATE clientRecord SET recordDate = ? WHERE id = ? ',
      [clientRecordModel.recordDate.toIso8601String(), clientRecordModel.id],
    );

    return {'result': 'success'};
  }

  Future<Map> createNewClientRecord(
      ClientRecord record, List<Service> selectedServices) async {
    print(record);
    selectedServices.forEach((Service service) {
      record.recordDuration += service.duration;
    });
    var dateStart = new DateTime(record.recordDate.year, record.recordDate.month, record.recordDate.day, 00, 00);
    var dateEnd =
        record.recordDate.add(new Duration(minutes: record.recordDuration));

    print(record.masterId);
    print(dateStart);
    List<Map<String, dynamic>> masterWork = await _database.rawQuery(
      'SELECT COUNT(1) as count FROM employeeWorkDays WHERE isWorkDay = 1 AND employeeId = ? AND date BETWEEN ? AND ?',
      [
        record.masterId,
        dateStart.toIso8601String(),
        dateEnd.toIso8601String()
      ],
    );

    print(masterWork);

    if (masterWork[0]['count'] == 0) {
      return {'result': 'errorNotWork', 'id': 0};
    }

    List<Map<String, dynamic>> allow = await _database.rawQuery(
      'SELECT COUNT(1) as count FROM clientRecord WHERE masterId = ? AND recordDate BETWEEN ? AND ?',
      [
        record.masterId,
        record.recordDate.toIso8601String(),
        dateEnd.toIso8601String()
      ],
    );

    if (allow[0]['count'] > 0) {
      return {'result': 'error', 'id': 0};
    }

    var clientData = await _database.rawQuery(
      'SELECT recordCount FROM clients WHERE id = ? ',
      [record.clientId],
    );
    await _database.rawQuery(
      'UPDATE clients SET recordCount = ? WHERE id = ? ',
      [(clientData[0]['recordCount'] + 1), record.clientId],
    );

    int recordId = await _database.insert('clientRecord', record.toMap());
    var batch = _database.batch();
    selectedServices.forEach((Service service) {
      batch.insert(
          'recordServices', {'recordId': recordId, 'serviceId': service.id});
    });
    batch.commit();
    return {'result': 'success', 'id': recordId};
  }

  Future removeClientRecord(ClientRecord clientRecord) async {
    var clientData = await _database.rawQuery(
      'SELECT recordCount FROM clients WHERE id = ? ',
      [clientRecord.clientId],
    );
    await _database.rawQuery(
      'UPDATE clients SET recordCount = ? WHERE id = ? ',
      [(clientData[0]['recordCount'] - 1), clientRecord.clientId],
    );
    return await _database
        .delete('clientRecord', where: 'id = ?', whereArgs: [clientRecord.id]);
  }
  /* END CLIENT RECORDS */

  /* BEGIN FINANCE */

  Future<List<Finance>> getFinanceList() async {
    List<Map<String, dynamic>> maps = await _database.query('finance');

    return List.generate(maps.length, (i) {
      return Finance(
        id: maps[i]['id'],
        financeType: maps[i]['financeType'],
        dateCreate: DateTime.parse(maps[i]['dateCreate']),
        money: maps[i]['money'],
        comment: maps[i]['comment'],
        nameWhoPay: maps[i]['nameWhoPay'],
        recordId: maps[i]['recordId'],
        masterId: maps[i]['masterId'],
      );
    });
  }

  Future<Map> getFinanceData() async {
    List<Map<String, dynamic>> totalIncome = await _database.rawQuery(
      'SELECT SUM(finance.money) as money FROM finance WHERE financeType = ?',
      [Finance.income]
    );
    List<Map<String, dynamic>> totalSales = await _database.rawQuery(
      'SELECT SUM(finance.money) as money FROM finance WHERE financeType = ? AND recordId IS NULL',
      [Finance.income]
    );
    List<Map<String, dynamic>> totalServices = await _database.rawQuery(
      'SELECT SUM(finance.money) as money FROM finance WHERE financeType = ? AND recordId IS NOT NULL',
      [Finance.income]
    );

    return {
      'totalIncome': totalIncome[0]['money'] ?? 0,
      'totalSales': totalSales[0]['money'] ?? 0,
      'totalServices': totalServices[0]['money'] ?? 0,
    };
  }

  Future<Finance> addPaymentForService(
      int summ, ClientRecordModel clientRecord) async {
    Finance finance = new Finance(
        financeType: Finance.income,
        money: summ,
        comment: 'Услуга',
        nameWhoPay: clientRecord.clientName,
        clientId: clientRecord.clientId,
        masterId: clientRecord.masterId,
        recordId: clientRecord.id);
    int insertedId = await _insertFinance(finance);
    finance.id = insertedId;
    updateClientRecordStatus(clientRecord, ClientRecord.paidStatus);
    return finance;
  }

  Future updateProductsSupply(List<Product> productList, int totalMoney) async {
    var batch = _database.batch();
    productList.forEach((Product product) async {
      var productFromDB = await _database
          .query('product', where: 'id = ?', whereArgs: [product.id]);
      batch.update(
          'product',
          {
            'costSelf': product.costSelf,
            'quantity': product.quantity + productFromDB[0]['quantity'],
          },
          where: 'id = ?',
          whereArgs: [product.id]);
    });
    batch.commit();
    Finance finance = new Finance(
      financeType: Finance.outcome,
      money: totalMoney,
      comment: 'Закупка товаров',
    );
    await _insertFinance(finance);
  }

  Future updateProductSell(Product product, int totalMoney) async {
    var batch = _database.batch();
    var productFromDB = await _database
        .query('product', where: 'id = ?', whereArgs: [product.id]);
    batch.update(
      'product',
      {
        'quantity': productFromDB[0]['quantity'] - product.quantity,
      },
      where: 'id = ?',
      whereArgs: [product.id],
    );
    batch.commit();
    Finance finance = new Finance(
      financeType: Finance.income,
      money: totalMoney,
      comment: 'Продажа товаров',
    );
    await _insertFinance(finance);
  }

  Future updateProductWriteOff(Product product, int writeOffCount) async {
    var productFromDB = await _database
        .query('product', where: 'id = ?', whereArgs: [product.id]);
    int decreasingQuantity = 0;
    int newCapacity;
    if (productFromDB[0]['writeOffCount'] >= writeOffCount) {
      newCapacity = productFromDB[0]['writeOffCapacity'] - writeOffCount;
    } else {
      decreasingQuantity++;
      while (writeOffCount > productFromDB[0]['writeOffCapacity']) {
        decreasingQuantity++;
        writeOffCount -= productFromDB[0]['writeOffCapacity'];
      }
      newCapacity = productFromDB[0]['writeOffCapacity'] - writeOffCount;
    }
    _database.update(
      'product',
      {
        'quantity': productFromDB[0]['quantity'] - decreasingQuantity,
        'writeOffCount': newCapacity,
      },
      where: 'id = ?',
      whereArgs: [product.id],
    );
  }

  Future addConsumption(String comment, int totalMoney) async {
    Finance finance = new Finance(
      financeType: Finance.outcome,
      money: totalMoney,
      comment: comment,
    );
    await _insertFinance(finance);
  }

  Future<int> calculateSalary(
      Employee employee, DateTime dateFrom, DateTime dateTo) async {
    double percent = employee.salaryPercent / 100;
    List<Map<String, dynamic>> totalMoneyForServices = await _database.rawQuery(
      'SELECT SUM(money) as money FROM finance WHERE masterId = ? AND dateCreate BETWEEN ? AND ?',
      [employee.id, dateFrom.toIso8601String(), dateTo.toIso8601String()],
    );
    if (totalMoneyForServices[0]['money'] == null) {
      return 0;
    }
    return (percent * totalMoneyForServices[0]['money']).toInt();
  }

  Future<int> _insertFinance(Finance finance, {batch}) async {
    return await _database.insert('finance', finance.toMap());
  }
  /* END FINANCE */

  // BEGIN SERVICES
  Future<List<Service>> serviceList({List ids}) async {
    List<Map<String, dynamic>> maps;
    if (ids == null) {
      maps = await _database.query('services', orderBy: 'name ASC');
    } else {
      maps = await _database.query('services', where: 'id IN', whereArgs: ids, orderBy: 'name ASC');
    }

    return List.generate(maps.length, (i) {
      return Service(
        id: maps[i]['id'],
        name: maps[i]['name'],
        cost: maps[i]['cost'],
        duration: maps[i]['duration'],
        comment: maps[i]['comment'],
      );
    });
  }

  Future<int> removeService(int serviceId) async {
    return await _database
        .delete('services', where: 'id = ?', whereArgs: [serviceId]);
  }

  Future<int> updateService(Service service) async {
    return await _database.update('services', service.toMap(),
        where: 'id = ?', whereArgs: [service.id]);
  }

  Future<int> insertService(Service service) async {
    return await _database.insert('services', service.toMap());
  }
  // END SERVICES

  // BEGIN EMPLOYEE

  Future<List<Employee>> getWorkingEmployeeList(DateTime date) async {
    DateTime dateFrom = new DateTime(date.year, date.month, date.day, 00, 00);
    DateTime dateTo = new DateTime(date.year, date.month, date.day, 23, 59);
    final List<Map<String, dynamic>> maps =
        await _database.rawQuery('''SELECT employee.id as id, employee.name as name
        FROM employee
        INNER JOIN employeeWorkDays ON employeeWorkDays.employeeId = employee.id
        WHERE employeeWorkDays.date BETWEEN ? AND ?
        GROUP BY employee.id
        ''', [dateFrom.toIso8601String(), dateTo.toIso8601String()]);
    // print(date.toIso8601String());
    // print(dateTo.toIso8601String());
    print(maps);
    return List.generate(maps.length, (i) {
      return Employee(
        id: maps[i]['id'],
        name: maps[i]['name'],
      );
    });
  }

  Future<List<Employee>> employeeList() async {
    final List<Map<String, dynamic>> maps = await _database.query('employee');

    return List.generate(maps.length, (i) {
      return Employee(
        id: maps[i]['id'],
        name: maps[i]['name'],
        phone: maps[i]['phone'],
        role: maps[i]['role'],
        speciality: maps[i]['speciality'],
        birthday: maps[i]['birthday'] != null
            ? DateTime.parse(maps[i]['birthday'])
            : null,
        gender: maps[i]['gender'],
        salaryPercent: maps[i]['salaryPercent'],
        salaryValue: maps[i]['salaryValue'],
      );
    });
  }

  Future<Employee> getEmployee(int id) async {
    final List<Map<String, dynamic>> maps =
        await _database.query('employee', where: 'id = ?', whereArgs: [id]);
    return Employee(
      id: maps[0]['id'],
      name: maps[0]['name'],
      phone: maps[0]['phone'],
    );
  }

  Future<List<EmployeeWorkDay>> getEmployeeWorkDays(Employee employee) async {
    final List<Map<String, dynamic>> maps = await _database
        .query('employeeWorkDays', where: 'employeeId = ?', whereArgs: [employee.id]);
    return List.generate(maps.length, (i) {
      return EmployeeWorkDay(
        id: maps[i]['id'],
        date: DateTime.parse(maps[i]['date']),
        isWorkDay: maps[i]['isWorkDay'],
        employeeId: maps[i]['employeeId'],
      );
    });
  }

  Future<int> addEmployeeWorkDay(DatePeriod datePeriod, int isWorkDay, Employee employee) async {
    DateTime startDate = datePeriod.start;
    DateTime endDate = datePeriod.end;
    await _database.delete('employeeWorkDays', where: 'employeeId = ? AND date BETWEEN ? AND ?', whereArgs: [employee.id, startDate.toIso8601String(), endDate.toIso8601String()]);

    if (datePeriod.start.day == datePeriod.end.day) {
      EmployeeWorkDay workDay = new EmployeeWorkDay(date: startDate, isWorkDay: isWorkDay, employeeId: employee.id);
      _database.insert('employeeWorkDays', workDay.toMap());
    } else {
      var batch = _database.batch();
      while(startDate.isBefore(endDate)) {
        EmployeeWorkDay workDay = new EmployeeWorkDay(date: startDate, isWorkDay: isWorkDay, employeeId: employee.id);
        batch.insert('employeeWorkDays', workDay.toMap());
        startDate = startDate.add(Duration(days: 1));
      }
      await batch.commit();
    }
  }

  Future<int> removeEmployee(int id) async {
    return await _database.delete('employee', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> updateEmployee(Employee employee) async {
    return await _database.update('employee', employee.toMap(),
        where: 'id = ?', whereArgs: [employee.id]);
  }

  Future<int> insertEmployee(Employee employee) async {
    return await _database.insert('employee', employee.toMap());
  }
  // END EMPLOYEE

  // BEGIN PRODUCTS
  Future<List<Product>> productList() async {
    final List<Map<String, dynamic>> maps = await _database.query('product', orderBy: 'name ASC');

    return List.generate(maps.length, (i) {
      return Product(
        id: maps[i]['id'],
        name: maps[i]['name'],
        quantity: maps[i]['quantity'],
        costSelf: maps[i]['costSelf'],
        costSell: maps[i]['costSell'],
        writeOffCount: maps[i]['writeOffCount'],
        writeOffCapacity: maps[i]['writeOffCapacity'],
      );
    });
  }

  Future<int> removeProduct(int id) async {
    return await _database.delete('product', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> updateProduct(Product product) async {
    return await _database.update('product', product.toMap(),
        where: 'id = ?', whereArgs: [product.id]);
  }

  Future<int> insertProduct(Product product) async {
    return await _database.insert('product', product.toMap());
  }

  // END PRODUCTS

Future<int> insertConsumptionType(ConsumptionType consumptionType) async {
    return await _database.insert('consumptionType', consumptionType.toMap());
  }

  Future<List<ConsumptionType>> consumptionTypeList() async {
    final List<Map<String, dynamic>> maps = await _database.query('consumptionType');

    return List.generate(maps.length, (i) {
      return ConsumptionType(
        id: maps[i]['id'],
        name: maps[i]['name']
      );
    });
  }
  void generateRecordEvents() {
    int lastDay = 30;
    var batch = _database.batch();

    Service service1 = Service(id: 1, duration: 60);
    Service service2 = Service(id: 2, duration: 90);
    Service service3 = Service(id: 3, duration: 120);
    Service service4 = Service(id: 4, duration: 60);

    var rng = new Random();
    for (var day = 1; day < lastDay; day++) {
      createNewClientRecord(
          ClientRecord(
            clientId: 1,
            masterId: 1,
            recordDate: new DateTime(2020, 5, day, 8 + rng.nextInt(3), 30, 00),
            recordDuration: 0,
            recordStatus: 'не оплачен',
            serviceCost: 1200,
          ),
          [service1, service2]);
      createNewClientRecord(
          ClientRecord(
            clientId: 2,
            masterId: 1,
            recordDate: new DateTime(2020, 5, day, 16 + rng.nextInt(2), 30, 00),
            recordDuration: 0,
            recordStatus: 'не оплачен',
            serviceCost: 1200,
          ),
          [service2, service3]);

      createNewClientRecord(
          ClientRecord(
            clientId: 1,
            masterId: 2,
            recordDate: new DateTime(2020, 5, day, 12 + rng.nextInt(2), 30, 00),
            recordDuration: 0,
            recordStatus: 'не оплачен',
            serviceCost: 1200,
          ),
          [service1, service2]);

      createNewClientRecord(
          ClientRecord(
            clientId: 1,
            masterId: 3,
            recordDate: new DateTime(2020, 5, day, 8 + rng.nextInt(2), 30, 00),
            recordDuration: 0,
            recordStatus: 'не оплачен',
            serviceCost: 1200,
          ),
          [service2, service3, service4]);

      createNewClientRecord(
          ClientRecord(
            clientId: 1,
            masterId: 3,
            recordDate: new DateTime(2020, 5, day, 16 + rng.nextInt(2), 30, 00),
            recordDuration: 0,
            recordStatus: 'не оплачен',
            serviceCost: 1200,
          ),
          [service2]);
    }

    for (var day = 17; day < lastDay; day++) {
      createNewClientRecord(
          ClientRecord(
            clientId: 1,
            masterId: 3,
            recordDate: new DateTime(2020, 6, day, 10 + rng.nextInt(2), 30, 00),
            recordDuration: 0,
            recordStatus: 'не оплачен',
            serviceCost: 1200,
          ),
          [Service(id: 2, duration: 90)]);
    }

    batch.commit();
  }
}
