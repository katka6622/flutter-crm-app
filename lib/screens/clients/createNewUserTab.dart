// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/models/client.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class CreateNewUserTab extends StatefulWidget {
  static const title = 'Новый клиент';
  static const androidIcon = Icon(Icons.add);

  @override
  NewUserState createState() => NewUserState();
}

class NewUserState extends State<CreateNewUserTab> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  Client newClient = new Client();
  String genderValue = ConstantValues.gengerList[0];

  @override
  Widget build(BuildContext context) {
    DatabaseHelper dbHelper = DatabaseHelper.instance;
    return Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text(CreateNewUserTab.title),
        ),
        body: buildForm(context),
        bottomNavigationBar: Container(
            height: 50,
            margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
            child: RaisedButton(
                onPressed: () => {
                      if (_formKey.currentState.validate())
                        {
                          _formKey.currentState.save(),
                          dbHelper.insertClient(newClient),
                          Navigator.pop(context, 'updateState'),
                        }
                    },
                color: Color.fromRGBO(68, 192, 192, 1),
                child: new Text(
                  'СОЗДАТЬ',
                  style: TextStyle(fontSize: 18.0),
                ))));
  }

  final _formKey = GlobalKey<FormState>();

  Widget buildForm(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[ 
              _nameField(),
              commonFormPadding(),
              Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
                _genderField(),
              ]),
              commonFormPadding(),
              _phoneField(),
              commonFormPadding(),
              _birthdayField(),
            ]),
          ),
        );
  }

  Widget _nameField() {
    return Container(
      padding: EdgeInsets.only(top: 10.0),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Имя:', style: TextStyle(fontWeight: FontWeight.bold)),
            TextFormField(
        decoration: const InputDecoration(
          hintText: 'Введите имя клиента',
        ),
        validator: (value) {
          if (value.isEmpty) {
            return 'Поле обязательно к заполнению';
          }
          return null;
        },
        onSaved: (String value) {
          newClient.name = value;
        })]));
        
  }

  Widget _phoneField() {
    return phoneFormField('', (String value) {
      newClient.phone = value;
    });
  }

  Widget _birthdayField() {
    final format = DateFormat("dd-MM-yyyy");
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children:  <Widget>[
      Text('День рождения', style: TextStyle(fontWeight: FontWeight.bold)),
      DateTimeField(
        format: format,
        onShowPicker: (context, currentValue) {
          return showDatePicker(
              context: context,
              firstDate: DateTime(1900),
              initialDate: currentValue ?? DateTime.now(),
              lastDate: DateTime(2100));
        },
        onSaved: (DateTime birthday) {
          newClient.birthday = birthday;
        },
      ),
    ]);
  }

  Widget _genderField() {
    var callback = (String newValue) {
      Client var1 = newClient;
      setState(() {
        genderValue = newValue;
        var1.gender = newValue;
        newClient = var1;
      });
    };
    newClient.gender = genderValue;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Пол:', style: TextStyle(fontWeight: FontWeight.bold)),
        dropdownField(ConstantValues.gengerList, callback, genderValue),
      ],
    );
  }
}
