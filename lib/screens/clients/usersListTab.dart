// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';

import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/drawer.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/client.dart';
import 'package:flutter_crm_app/screens/clients/userInfoTab.dart';
import 'package:flutter_crm_app/screens/clients/createNewUserTab.dart';

class UsersListTab extends StatefulWidget {
  static const title = 'Клиенты';
  static const androidIcon = Icon(Icons.group);

  @override
  UsersListState createState() => UsersListState();
}

class UsersListState extends State<UsersListTab> {
  final _scaffoldMainKey = GlobalKey<ScaffoldState>();
  Future<List<Client>> clients;
  DatabaseHelper helper = DatabaseHelper.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldMainKey,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('ВСЕ КЛИЕНТЫ', style: TextStyle(fontSize: 16.0)),
      ),
      drawer: AndroidDrawer(), //call
      body: buildUsersList(),
      floatingActionButton: Theme(
          data: Theme.of(context).copyWith(
            colorScheme: Theme.of(context)
                .colorScheme
                .copyWith(secondary: Color.fromRGBO(68, 192, 192, 1)),
          ),
          child: FloatingActionButton(
            // When the user presses the button, show an alert dialog containing the
            // text that the user has entered into the text field.
            onPressed: () {
              _navigateToCreate(context, _scaffoldMainKey);
            },
            tooltip: 'Show me the value!',
            child: Icon(Icons.add),
          )),
    );
  }

  Future<List<Client>> _loadClients(DatabaseHelper helper) async {
    return await helper.clients();
  }

  Widget buildUsersList() {
    clients = _loadClients(helper);
    return FutureBuilder<List<Client>>(
        future: clients,
        builder: (context, snapshot) {
          List<Client> clients = snapshot.data ?? [];
          return ListView.builder(
              padding: EdgeInsets.all(0),
              itemCount: clients.length,
              itemBuilder: (context, index) {
                Client client = clients[index];
                //add  condition for last Divider
                // if (index.isOdd && index < 50) return Divider();

                return _buildRowNew(client);
              });
        });
  }

  Widget _buildUserInfoRow(Client client) {
    return Container(
        decoration: BoxDecoration(
            // border: Border.all(color: Colors.grey[300]),
            border: Border.all(color: Colors.grey[200]),
            color: Colors.grey[50]),
        width: double.infinity,
        height: 80,
        padding: EdgeInsets.only(left: 20.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(client.name,
                  style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0)),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Row(children: [
                  Icon(Icons.call,
                      color: Color.fromRGBO(68, 192, 192, 1), size: 12.0),
                  Padding(padding: EdgeInsets.only(right: 7)),
                  Text(formatPhone(client.phone),
                      style: TextStyle(fontSize: 14.0)),
                ]),
                Icon(Icons.more_vert, color: Color.fromRGBO(68, 192, 192, 1))
              ]),
              Text("Визиты:" + client.recordCount.toString(),
                  style: TextStyle(color: Colors.grey, fontSize: 12.0)),
            ]));
  }

  Widget _buildRowNew(Client client) {
    return GestureDetector(
        onTap: () {
          _navigateToEdit(context, client);
        },
        child: Container(
          child: _buildUserInfoRow(client),
        ));
  }

  _navigateToCreate(BuildContext context, _scaffoldMainKey) async {
    var createStatus = await Navigator.push(_scaffoldMainKey.currentContext,
        MaterialPageRoute(builder: (context) => new CreateNewUserTab()));

    if (createStatus == 'updateState') {
      setState(() {});
    }
  }

  _navigateToEdit(BuildContext context, Client client) async {
    var updateStatus = await Navigator.push(context,
        MaterialPageRoute(builder: (context) => UserInfoTab(client: client)));
    if (updateStatus == 'updateState') {
      setState(() {});
    }
  }
}
