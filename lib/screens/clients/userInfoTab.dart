// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_crm_app/models/client.dart';
import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';

class UserInfoTab extends StatefulWidget {
  static const title = 'Данные клиента';
  static const androidIcon = Icon(Icons.list);
  final Client client;

  const UserInfoTab({Key key, this.client}) : super(key: key);

  @override
  UserInfoState createState() => UserInfoState();
}

class UserInfoState extends State<UserInfoTab> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final smsText =
      'Привет%20салон%20ВсеяКетрин%20приглашает%20вас%20на%20ноготочки';
  DatabaseHelper helper = DatabaseHelper.instance;
  int averageCheck = 0;
  int totalVisitCount = 0;
  int totalMoneySpend = 0;

  Future<void> _loadUserData(Client client) async {
    totalMoneySpend = await helper.clientRecordMoneySpend(client.id);
    averageCheck = await helper.clientRecordAverageCost(client.id);
    totalVisitCount = await helper.clientRecordCount(client.id);
  }

  @override
  Widget build(BuildContext context) {
    _loadUserData(widget.client);
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(UserInfoTab.title),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.delete_outline,
                color: Colors.red[400],
              ),
              onPressed: () => {
                _formKey.currentState.save(),
                helper.removeClient(widget.client.id),
                Navigator.pop(context, 'updateState'),
              },
            ),
          ],
        ),
        backgroundColor: Colors.yellow[50],
        body: SingleChildScrollView(
          child: FutureBuilder(
              future: _loadUserData(widget.client),
              builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
                return pageBuilder(context);
              }),
        ),
        bottomNavigationBar: Container(
            height: 50,
            margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
            child: RaisedButton(
                onPressed: () => {
                      if (_formKey.currentState.validate())
                        {
                          _formKey.currentState.save(),
                          helper.updateClient(widget.client),
                          Navigator.pop(context, 'updateState'),
                        }
                    },
                color: Color.fromRGBO(68, 192, 192, 1),
                child: new Text(
                  'СОХРАНИТЬ',
                  style: TextStyle(fontSize: 18.0),
                ))));
  }

  final _formKey = GlobalKey<FormState>();

  Widget pageBuilder(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _clientInfoBlock(),
              Padding(padding: const EdgeInsets.all(10)),
              _signInfoBlock(),
              Padding(padding: const EdgeInsets.all(10)),
              _personalInfoBlock(),
            ],
          ),
        ));
  }

  /* BEGIN CLIENT BLOCK */
  Widget _clientInfoBlock() {
    return styledContainer(
        customPadding: EdgeInsets.all(16.0),
        content: Column(
          children: <Widget>[
            clientName(),
            commonFormPadding(),
            clientPhone(),
            commonFormPadding(),
            clientBirthday(),
            commonFormPadding(),
            clientActions(),
          ],
        ));
  }

  Widget clientName() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Имя:', style: TextStyle(fontWeight: FontWeight.bold)),
        TextFormField(
            initialValue: widget.client.name,
            decoration: const InputDecoration(
              hintText: 'Имя клиента',
            ),
            validator: (value) {
              if (value.isEmpty) {
                return 'Поле обязательно к заполнению';
              }
              return null;
            },
            onSaved: (String value) {
              widget.client.name = value;
            }),
      ],
    );
  }

  Widget clientPhone() {
    return phoneFormField(widget.client.phone, (String value) {
      widget.client.phone = value;
    });
  }

  Widget clientBirthday() {
    final format = DateFormat("dd-MM-yyyy");
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('День рождения', style: TextStyle(fontWeight: FontWeight.bold)),
          DateTimeField(
            initialValue: widget.client.birthday,
            format: format,
            onShowPicker: (context, currentValue) {
              return showDatePicker(
                  context: context,
                  firstDate: DateTime(1900),
                  initialDate: widget.client.birthday ?? DateTime.now(),
                  lastDate: DateTime(2100));
            },
            onSaved: (DateTime birthday) {
              widget.client.birthday = birthday;
            },
          ),
        ]);
  }

  Widget clientActions() {
    return Container(
        padding: EdgeInsets.fromLTRB(5, 30, 5, 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Column(
              children: <Widget>[
                launchRaisButton(
                    "Позвонить", "tel://" + widget.client.phone, Icons.call),
              ],
            ),
            Column(
              children: <Widget>[
                launchRaisButton(
                    "Написать",
                    "sms:" + widget.client.phone + "?body=" + smsText,
                    Icons.message),
              ],
            ),
          ],
        ));
  }

  /* END CLIENT BLOCK */

  /* BEGIN SIGN INFO BLOCK */
  Widget _signInfoBlock() {
    final lastVisit = widget.client.lastRecord != null
        ? DateFormat('dd-MM-yyyy kk:mm').format(widget.client.lastRecord)
        : 'нет';
    return styledContainer(
      customPadding: EdgeInsets.all(5),
      content: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                "Всего визитов: " + totalVisitCount.toString(),
                style: TextStyle(fontSize: 14),
              )
            ],
          ),
          Row(children: <Widget>[Text("Последний визит: " + lastVisit)]),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Предстоящие визиты отсутствуют"),
              launchRaisedButton("Пригласить", 14.00,
                  "sms:" + widget.client.phone + "?body=" + smsText)
            ],
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            metricCell(
                text: 'Потрачено',
                value: formatMoney(totalMoneySpend).toString()),
            metricCell(
                text: 'Средний чек',
                value: formatMoney(averageCheck).toString())
          ]),
        ],
      ),
    );
  }
  /* END SIGN INFO BLOCK */

  
  Widget _personalInfoBlock() {
    return styledContainer(
        customPadding: EdgeInsets.all(5),
        content: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // Padding(padding: const EdgeInsets.symmetric(horizontal: 20.0)),
            Text('Примечание',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
            // Text(widget.client.comment, style: TextStyle(fontSize: 14)),
            // TextFormField(initialValue: widget.client.comment, enableSuggestions: true,),

            TextFormField(
                initialValue: widget.client.comment,
                enableSuggestions: true,
                minLines: 1,
                maxLines: 2,
                decoration: const InputDecoration(
                  hintText: '',
                ),
                onSaved: (String value) {
                  widget.client.comment = value;
                }),
          ],
        ));
  }
  /* END personal block */

  Widget metricCell({String text, String value}) {
    return Container(
        width: 160,
       
        child: Column(children: <Widget>[Text(text), Text(value)]));
  }

  Widget styledContainer({Widget content, EdgeInsets customPadding}) {
    final padding = customPadding ?? EdgeInsets.all(5);
    return Container(
        padding: padding,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: content);
  }
}
