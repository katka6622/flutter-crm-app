import 'package:flutter/material.dart';
import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/drawer.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/product.dart';
import 'package:flutter_crm_app/screens/products/addProductSup.dart';
import 'package:flutter_crm_app/screens/products/createNewSale.dart';
import 'package:flutter_crm_app/screens/products/addProductSup.dart';
import 'package:flutter_crm_app/screens/products/createNewWriteOff.dart';
import 'package:flutter_crm_app/screens/products/supplyList.dart';

class TrafficProductTab extends StatefulWidget {
  static const title = 'Движение товара';
  static const androidIcon = Icon(Icons.list);

  @override
  TrafficProductState createState() => TrafficProductState();
}

class TrafficProductState extends State<TrafficProductTab> {
  final _scaffoldMainKey = GlobalKey<ScaffoldState>();
  DatabaseHelper helper = DatabaseHelper.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldMainKey,
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text(TrafficProductTab.title),
        actions: <Widget>[],
      ),
      
      body: buildTraffic(context),
    );
  }

  final _formKey = GlobalKey<FormState>();

  Widget buildTraffic(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _supplyButton(),
              commonFormPadding(),
              _saleButton(),
              commonFormPadding(),
              writeOffButton()
            ]),
      ),
    );
  }

  Widget _supplyButton() {
    return Center(
      child: RaisedButton(
        onPressed: () {
          _navigateToSupply(context);
        },
        child: Text(
          'Поставка',
          style: TextStyle(fontSize: 16.0),
        ),
        color: Color.fromRGBO(68, 192, 192, 1),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5))),
        
      ),
    );
  }

  _navigateToSupply(BuildContext context) async {
    var createResult = await Navigator.push(_scaffoldMainKey.currentContext,
        MaterialPageRoute(builder: (context) => new SupplyListTab()));

    if (createResult == 'sypply') {
      setState(() {});
    }
  }

  Widget _saleButton() {
    return Center(
      child: RaisedButton(
        onPressed: () {
          _navigateToSale(context);
        },
        child: Text(
          'Продажа',
          style: TextStyle(fontSize: 16.0),
        ),
        color: Color.fromRGBO(68, 192, 192, 1),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5))),
      ),
    );
  }

  //
  _navigateToSale(BuildContext context) async {
    var createResult = await Navigator.push(_scaffoldMainKey.currentContext,
        MaterialPageRoute(builder: (context) => new CreateNewSale()));

    if (createResult == 'sypply') {
      setState(() {});
    }
  }

  Widget writeOffButton() {
    return Center(
      child: RaisedButton(
        onPressed: () {
          _writeOffSupply(context);
        },
        child: Text(
          'Списание',
          style: TextStyle(fontSize: 16.0),
        ),
        color: Color.fromRGBO(68, 192, 192, 1),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5))),
      ),
    );
  }

  //
  _writeOffSupply(BuildContext context) async {
    var createResult = await Navigator.push(_scaffoldMainKey.currentContext,
        MaterialPageRoute(builder: (context) => new CreateNewWriteOff()));

    if (createResult == 'sypply') {
      setState(() {});
    }
  }
}
