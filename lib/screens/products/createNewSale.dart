import 'package:flutter/material.dart';
import 'package:flutter_crm_app/models/baseModel.dart';
import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/product.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

class CreateNewSale extends StatefulWidget {
  static const title = 'Продажа товара';
  // static const androidIcon = Icon(Icons.add);

  @override
  NewSaleState createState() => NewSaleState();
}

class NewSaleState extends State<CreateNewSale> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  DatabaseHelper dbHelper = DatabaseHelper.instance;
  int summa = 0;
  Product selectedProduct = Product();
  List<int> selectedProductIds = [];
  Product newProduct = Product();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(CreateNewSale.title),
      ),
      body: buildForm(context),
      bottomNavigationBar: Container(
        height: 50,
        margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
        child: RaisedButton(
          onPressed: () => {
            if (_formKey.currentState.validate())
              {
                _formKey.currentState.save(),
                newProduct.name = selectedProduct.name,
                newProduct.id = selectedProduct.id,
                dbHelper.updateProductSell(newProduct, summa),
                Navigator.pop(context, newProduct),
              }
          },
          color: Color.fromRGBO(68, 192, 192, 1),
          child: new Text(
            'Оформить продажу',
            style: TextStyle(fontSize: 18.0),
          ),
        ),
      ),
    );
  }

  final _formKey = GlobalKey<FormState>();

  Widget buildForm(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _productField(),
              commonFormPadding(),
              _quantityField(),
              commonFormPadding(),
              _costSell(),
              commonFormPadding(),
              _totalCost(),
            ],
          ),
        ));
  }

  Widget _productField() {
    var callback = (value) {
      setState(() {
        selectedProduct = value;
      });
    };
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Товар',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        productSearchPopupField(dbHelper.productList(), "Выберите товар", "",
            selectedProduct, callback),
      ],
    );
  }

  Widget productSearchPopupField(
      futureList, hintText, searchHintText, selectedValue, callback) {
    return new FutureBuilder(
      future: futureList,
      builder: (context, snapshot) {
        List products = _convertToDropdown(snapshot.data) ?? [];
        return SearchableDropdown.single(
          items: products,
          value: selectedValue,
          hint: hintText,
          searchHint: searchHintText,
          onChanged: callback,
          isExpanded: true,
        );
      },
    );
  }

  Widget _costSell() {
    return Container(
      width: 300,
      child: Row( children: [
        Text('Цена за 1 ед:', style: TextStyle(fontWeight: FontWeight.bold)),
        Text(formatMoney(selectedProduct.costSell), style: TextStyle(fontWeight: FontWeight.bold)),
      ]),
    );
  }

  Widget _quantityField() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Количество:', style: TextStyle(fontWeight: FontWeight.bold)),
      TextFormField(
          decoration: const InputDecoration(
            hintText: 'Введите количество товаров',
          ),
          keyboardType: TextInputType.number,
          onChanged: (String value) {
            setState(() {
              summa = int.parse(value) * selectedProduct.costSell;
            });
          },
          validator: (value) {
            if (value.isEmpty) {
              return 'Поле обязательно к заполнению';
            }
            if (int.parse(value) > selectedProduct.quantity) {
              return 'Недостаточно товаров на складе';
            }
            return null;
          },
          onSaved: (String value) {
            newProduct.quantity = int.parse(value);
          }),
    ]);
  }

  Widget _totalCost() {
    return Container(
      width: 300,
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Text('Общая стоимость:', style: TextStyle(fontWeight: FontWeight.bold)),
        Text(formatMoney(summa), style: TextStyle(fontWeight: FontWeight.bold)),
      ]),
    );
  }

  List<DropdownMenuItem<BaseModel>> _convertToDropdown(List dataList) {
    List<DropdownMenuItem<BaseModel>> result = [];
    if (dataList != null) {
      dataList.forEach((element) {
        DropdownMenuItem<BaseModel> newElem =
            new DropdownMenuItem(value: element, child: Text(element.name));
        result.add(newElem);
      });
    }
    return result;
  }
}
