import 'package:flutter/material.dart';
import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/drawer.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/product.dart';
import 'package:flutter_crm_app/screens/products/addProductSup.dart';

class SupplyListTab extends StatefulWidget {
  static const title = 'Поступление товаров';
  static const androidIcon = Icon(Icons.local_mall);

  @override
  SupplyListState createState() => SupplyListState();
}

class SupplyListState extends State<SupplyListTab> {
  final _scaffoldMainKey = GlobalKey<ScaffoldState>();
  DatabaseHelper dbHelper = DatabaseHelper.instance;
  int summa = 0;
  List<Product> supplyList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldMainKey,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          title: Text(SupplyListTab.title),
          actions: <Widget>[],
        ),
        body: buildForm(context),
        floatingActionButton: Theme(
          data: Theme.of(context).copyWith(
            colorScheme: Theme.of(context)
                .colorScheme
                .copyWith(secondary: Color.fromRGBO(68, 192, 192, 1)),
          ),
          child: FloatingActionButton(
            onPressed: () {
              _navigateToCreate(context, _scaffoldMainKey);
            },
            child: Icon(Icons.add),
          )),
          bottomNavigationBar: Container(
            height: 50,
            margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
            child: RaisedButton(
                onPressed: () => {
                     if (_formKey.currentState.validate())
                        {
                          _formKey.currentState.save(),
                          dbHelper.updateProductsSupply(supplyList, summa),
                          Navigator.pop(context, 'updateState'),
                        }
                    },
                color: Color.fromRGBO(68, 192, 192, 1),
                child: new Text(
                  'СОХРАНИТЬ',
                  style: TextStyle(fontSize: 18.0),
                
        ))));
  }

  final _formKey = GlobalKey<FormState>();

  Widget buildForm(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(0.0),
        child: Form(
          key: _formKey,
            child: 
          Column(
            children: <Widget>[
              buildTable(),
              commonFormPadding(padding: 40),
              _totalCost(),
            ],
          ),
        ));
  }

  Widget buildTable() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: DataTable(
        columns: const <DataColumn>[
          DataColumn(
            label: Text(
              'Наименование',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          DataColumn(
            label: Text(
              'Количество',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          DataColumn(
            label: Text(
              'Цена за 1 ед',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          DataColumn(
            label: Text(
              'Стоимость',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ],
        rows: supplyList
            .map(
              (product) => DataRow(
                cells: <DataCell>[
                  DataCell(Container(
                    child: Text(product.name),
                    width: 100,
                  )),
                  DataCell(Text(product.quantity.toString())),
                  DataCell(Text(product.costSelf.toString())),
                  DataCell(
                      Text(formatMoney(product.quantity * product.costSelf))),
                ],
              ),
            )
            .toList(),
      ),
    );
  }

  _navigateToCreate(BuildContext context, _scaffoldMainKey) async {
    Product addedProduct = await Navigator.push(_scaffoldMainKey.currentContext,
        MaterialPageRoute(builder: (context) => new AddProductSup()));
    if (addedProduct != null) {
      var summary = addedProduct.quantity * addedProduct.costSelf;
      setState(() {
        supplyList.add(addedProduct);
        summa += summary;
      });
    }
  }

  Widget _totalCost() {
    return Container(
      width: 300,
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Text('Общая стоимость:', style: TextStyle(fontWeight: FontWeight.bold)),
        Text(formatMoney(summa), style: TextStyle(fontWeight: FontWeight.bold)),
      ]),
    );
  }
}
