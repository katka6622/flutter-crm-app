import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_crm_app/models/baseModel.dart';
import 'package:intl/intl.dart';

import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/product.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

class CreateNewWriteOff extends StatefulWidget {
  static const title = 'Списание товара';
  // static const androidIcon = Icon(Icons.add);
 

  @override
  NewWriteOffState createState() => NewWriteOffState();
}

class NewWriteOffState extends State<CreateNewWriteOff> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  DatabaseHelper dbHelper = DatabaseHelper.instance;
  Product selectedProduct = Product();
  List<int> selectedProductIds = [];
  Product newProduct = Product();
  int writeOffCount = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(CreateNewWriteOff.title),
      ),
      body: buildForm(context),
      bottomNavigationBar: Container(
        height: 50,
        margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
        child: RaisedButton(
          onPressed: () => {
            if (_formKey.currentState.validate())
              {
                _formKey.currentState.save(),
                newProduct.name = selectedProduct.name,
                newProduct.id = selectedProduct.id,
                dbHelper.updateProductWriteOff(newProduct, writeOffCount),
                Navigator.pop(context, newProduct),
              }
          },
          color: Color.fromRGBO(68, 192, 192, 1),
          child: new Text(
            'Списать',
            style: TextStyle(fontSize: 18.0),
          ),
        ),
      ),
    );
  }

  final _formKey = GlobalKey<FormState>();

  Widget buildForm(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _productField(),
              commonFormPadding(),
              _writeOffCountField(),
              commonFormPadding(),
            ],
          ),
        ));
  }

  Widget _productField() {
    var callback = (value) {
      setState(() {
        selectedProduct = value;
      });
    };
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Товар',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        productSearchPopupField(dbHelper.productList(), "Выберите товар", "",
            selectedProduct, callback),
      ],
    );
  }

  Widget productSearchPopupField(
      futureList, hintText, searchHintText, selectedValue, callback) {
    return new FutureBuilder(
      future: futureList,
      builder: (context, snapshot) {
        List products = _convertToDropdown(snapshot.data) ?? [];
        return SearchableDropdown.single(
          items: products,
          value: selectedValue,
          hint: hintText,
          searchHint: searchHintText,
          onChanged: callback,
          isExpanded: true,
        );
      },
    );
  }


  Widget _writeOffCountField() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Количество:', style: TextStyle(fontWeight: FontWeight.bold)),
      TextFormField(
          decoration: const InputDecoration(
            hintText: 'Объем или кол-во списания',
          ),
          keyboardType: TextInputType.number,
          validator: (value) {
            if (value.isEmpty) {
              return 'Поле обязательно к заполнению';
            }
            if (int.parse(value) > (selectedProduct.quantity * selectedProduct.writeOffCapacity)) {
              return 'Недостаточно товаров на складе';
            }
            return null;
          },
          onSaved: (String value) {
            writeOffCount = int.parse(value);
          }),
    ]);
  }

  

  List<DropdownMenuItem<BaseModel>> _convertToDropdown(List dataList) {
    List<DropdownMenuItem<BaseModel>> result = [];
    if (dataList != null) {
      dataList.forEach((element) {
        DropdownMenuItem<BaseModel> newElem =
            new DropdownMenuItem(value: element, child: Text(element.name));
        result.add(newElem);
      });
    }
    return result;
  }
}