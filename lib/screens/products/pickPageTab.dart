import 'package:flutter/material.dart';
import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/drawer.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/screens/products/productsListTab.dart';
import 'package:flutter_crm_app/screens/products/trafficProductTab.dart';

class PickPageTab extends StatefulWidget {
  static const title = 'Товары';
  static const androidIcon = Icon(Icons.local_mall);

  @override
  PickPageState createState() => PickPageState();
}

class PickPageState extends State<PickPageTab> {
  final _scaffoldMainKey = GlobalKey<ScaffoldState>();
  DatabaseHelper helper = DatabaseHelper.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldMainKey,
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text(PickPageTab.title),
        actions: <Widget>[],
      ),
      drawer: AndroidDrawer(), //call
      body: buildTraffic(context),
    );
  }

  final _formKey = GlobalKey<FormState>();

  Widget buildTraffic(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _listButton(),
              commonFormPadding(),
              _trafficButton(),
              commonFormPadding(),
              
            ]),
      ),
    );
  }

  Widget _listButton() {
    return Center(
      child: RaisedButton(
        onPressed: () {
          _navigateToTab(context);
        },
        child: Text(
          'Список товаров',
          style: TextStyle(fontSize: 16.0),
        ),
        color: Color.fromRGBO(68, 192, 192, 1),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5))),
        
      ),
    );
  }

  _navigateToTab(BuildContext context) async {
    await Navigator.push(_scaffoldMainKey.currentContext,
        MaterialPageRoute(builder: (context) => new ProductsListTab()));
  }

  Widget _trafficButton() {
    return Center(
      child: RaisedButton(
        onPressed: () {
          _navigateToTraffic(context);
        },
        child: Text(
          'Движение товаров',
          style: TextStyle(fontSize: 16.0),
        ),
        color: Color.fromRGBO(68, 192, 192, 1),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5))),
      ),
    );
  }

  //
  _navigateToTraffic(BuildContext context) async {
    await Navigator.push(_scaffoldMainKey.currentContext,
        MaterialPageRoute(builder: (context) => new TrafficProductTab()));
  }

}
