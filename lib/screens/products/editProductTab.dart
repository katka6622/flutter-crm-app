import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/product.dart';

class EditProductTab extends StatefulWidget {
  static const title = 'Продукт';
  static const androidIcon = Icon(Icons.list);
  final Product product;

  const EditProductTab({Key key, this.product}) : super(key: key);

  @override
  EditProductState createState() => EditProductState();
}

class EditProductState extends State<EditProductTab> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  DatabaseHelper dbHelper = DatabaseHelper.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text(EditProductTab.title),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.delete_outline,
                color: Colors.red[400],
              ),
              onPressed: () => {
                _formKey.currentState.save(),
                dbHelper.removeProduct(widget.product.id),
                Navigator.pop(context, 'updateState'),
              },
            ),
          ],
        ),
        body: buildForm(context),
        bottomNavigationBar: Container(
            height: 50,
            margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
            child: RaisedButton(
                onPressed: () => {
                      if (_formKey.currentState.validate())
                        {
                          _formKey.currentState.save(),
                          dbHelper.updateProduct(widget.product),
                          Navigator.pop(context, 'updateState'),
                        }
                    },
                color: Color.fromRGBO(68, 192, 192, 1),
                child: new Text(
                  'СОХРАНИТЬ',
                  style: TextStyle(fontSize: 18.0),
                ))));
  }

  final _formKey = GlobalKey<FormState>();

  Widget buildForm(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _nameField(),
              commonFormPadding(),
              _costSelfField(),
              commonFormPadding(),
              _costSellField(),
              commonFormPadding(),
              _capacityField()
            ]),
      ),
    );
  }

  Widget _nameField() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Наименование:', style: TextStyle(fontWeight: FontWeight.bold)),
      TextFormField(
          initialValue: widget.product.name,
          decoration: const InputDecoration(
            hintText: 'Название продукта',
          ),
          validator: (value) {
            if (value.isEmpty) {
              return 'Поле обязательно к заполнению';
            }
            return null;
          },
          onSaved: (String value) {
            widget.product.name = value;
          }),
    ]);
  }

  Widget _costSelfField() {
    var controller = MoneyMaskedTextController(
        rightSymbol: ' ₽',
        initialValue: widget.product.costSelf.toDouble(),
        thousandSeparator: ' ',
        decimalSeparator: '',
        precision: 0);
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Цена покупки:', style: TextStyle(fontWeight: FontWeight.bold)),
      TextFormField(
          controller: controller,
          decoration: const InputDecoration(
            hintText: 'Цена услуги (от)',
          ),
          keyboardType: TextInputType.number,
          validator: (value) {
            if (value.isEmpty) {
              return 'Поле обязательно к заполнению';
            }
            return null;
          },
          onSaved: (String value) {
            widget.product.costSelf = controller.numberValue.toInt();
          })
    ]);
  }

  Widget _costSellField() {
    var controller = MoneyMaskedTextController(
        rightSymbol: ' ₽',
        initialValue: widget.product.costSelf.toDouble(),
        thousandSeparator: ' ',
        decimalSeparator: '',
        precision: 0);
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Цена продажи:', style: TextStyle(fontWeight: FontWeight.bold)),
      TextFormField(
          controller: controller,
          decoration: const InputDecoration(
            hintText: 'Цена услуги (от)',
          ),
          keyboardType: TextInputType.number,
          validator: (value) {
            if (value.isEmpty) {
              return 'Поле обязательно к заполнению';
            }
            return null;
          },
          onSaved: (String value) {
            widget.product.costSelf = controller.numberValue.toInt();
          })
    ]);
  }

  Widget _capacityField() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Объем упаковки (гр/мл):',
          style: TextStyle(fontWeight: FontWeight.bold)),
      TextFormField(
          initialValue: widget.product.writeOffCapacity.toString(),
          decoration: const InputDecoration(
            hintText: 'Введите объем',
          ),
          keyboardType: TextInputType.number,
          validator: (value) {
            if (value.isEmpty) {
              return 'Поле обязательно к заполнению';
            }
            if (int.parse(value) == 0) {
              return 'Не должно быть меньше 1';
            }
            return null;
          },
          onSaved: (String value) {
            widget.product.writeOffCapacity = int.parse(value);
          }),
    ]);
  }
}
