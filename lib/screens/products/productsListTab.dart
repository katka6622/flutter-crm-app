import 'package:flutter/material.dart';

import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/drawer.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/product.dart';
import 'package:flutter_crm_app/screens/products/createNewProduct.dart';
import 'package:flutter_crm_app/screens/products/createNewSale.dart';
import 'package:flutter_crm_app/screens/products/createNewWriteOff.dart';
import 'package:flutter_crm_app/screens/products/editProductTab.dart';
import 'package:flutter_crm_app/screens/products/supplyList.dart';

class ProductsListTab extends StatefulWidget {
  static const title = 'Список товаров';
  static const androidIcon = Icon(Icons.local_mall);

  @override
  ProductsListState createState() => ProductsListState();
}

class ProductsListState extends State<ProductsListTab> {
  final _scaffoldMainKey = GlobalKey<ScaffoldState>();
  Future<List<Product>> productList;
  DatabaseHelper helper = DatabaseHelper.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldMainKey,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          title: Text(ProductsListTab.title),
          actions: <Widget>[],
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: buildTable(),
        ),
        floatingActionButton: Theme(
          data: Theme.of(context).copyWith(
            colorScheme: Theme.of(context)
                .colorScheme
                .copyWith(secondary: Color.fromRGBO(68, 192, 192, 1)),
          ),
          child: FloatingActionButton(
            onPressed: () {
              _navigateToCreate(context, _scaffoldMainKey);
            },
            child: Icon(Icons.add),
          ),
        ));
  }

  Widget buildTable() {
    return FutureBuilder(
        future: _loadData(),
        builder: (context, snapshot) {
          List<Product> products = snapshot.data ?? [];
          return SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: DataTable(
              columns: const <DataColumn>[
                DataColumn(
                  label: Text(
                    'Наименование',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                DataColumn(
                  label: Text(
                    'Количество',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                DataColumn(
                  label: Text(
                    'Доступно для списания',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                DataColumn(
                  label: Text(
                    'Цена покупки',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                DataColumn(
                  label: Text(
                    'Цена продажи',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ],
              rows: products
                  .map(
                    (product) => DataRow(
                      cells: <DataCell>[
                        DataCell(
                            Container(
                              child: Text(product.name),
                              width: 150,
                            ), onTap: () {
                          _navigateToEdit(context, product);
                        }),
                        DataCell(Text(product.quantity.toString())),
                        DataCell(Text(product.writeOffCount.toString() + ' / ' + product.writeOffCapacity.toString())),
                        DataCell(Text(product.costSelf.toString())),
                        DataCell(Text(product.costSell.toString())),
                      ],
                    ),
                  )
                  .toList(),
            ),
          );
        });
  }

  Future<List<Product>> _loadData() async {
    return await helper.productList();
  }

  _navigateToCreate(BuildContext context, _scaffoldMainKey) async {
    var createResult = await Navigator.push(_scaffoldMainKey.currentContext,
        MaterialPageRoute(builder: (context) => new CreateNewProduct()));

    if (createResult == 'created') {
      setState(() {});
    }
  }

  _navigateToEdit(BuildContext context, Product product) async {
    var updatedStatus = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => EditProductTab(product: product)),
    );
    if (updatedStatus == 'updateState') {
      setState(() {});
    }
  }
}
