import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:intl/intl.dart';

import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/product.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class CreateNewProduct extends StatefulWidget {
  static const title = 'Добавление товара';
  static const androidIcon = Icon(Icons.add);

  @override
  NewProductState createState() => NewProductState();
}

class NewProductState extends State<CreateNewProduct> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  Product newProduct = Product();
  int state = 0;
  Widget _animatedWidget;

  @override
  void initState() {
    super.initState();
    _animatedWidget = _costSellField();
  }

  @override
  Widget build(BuildContext context) {
    DatabaseHelper dbHelper = DatabaseHelper.instance;
    return Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text(CreateNewProduct.title),
        ),
        body: buildForm(context),
        bottomNavigationBar: Container(
            height: 50,
            margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
            child: RaisedButton(
                onPressed: () => {
                      if (_formKey.currentState.validate())
                        {
                          _formKey.currentState.save(),
                          dbHelper.insertProduct(newProduct),
                          Navigator.pop(context, 'created'),
                        }
                    },
                color: Color.fromRGBO(68, 192, 192, 1),
                child: new Text(
                  'СОЗДАТЬ',
                  style: TextStyle(fontSize: 18.0),
                ))));
  }

  final _formKey = GlobalKey<FormState>();

  Widget buildForm(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _nameField(),
              commonFormPadding(),
              _costSelfField(),
              commonFormPadding(),
              _costSellField(),
              commonFormPadding(),
              _writeOffField(),
              commonFormPadding(),
              _capacityField(),
              // radioSwitcher(),
              // commonFormPadding(),
              // switcherWidget(),
            ],
          ),
        ));
  }

  Widget _nameField() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Наименование:', style: TextStyle(fontWeight: FontWeight.bold)),
      TextFormField(
          decoration: const InputDecoration(
            hintText: 'Введите наименование товара',
          ),
          validator: (value) {
            if (value.isEmpty) {
              return 'Поле обязательно к заполнению';
            }
            return null;
          },
          onSaved: (String value) {
            newProduct.name = value;
          }),
    ]);
  }

  Widget _costSelfField() {
    var controller = MoneyMaskedTextController(
        rightSymbol: ' ₽',
        initialValue: newProduct.costSelf.toDouble(),
        thousandSeparator: ' ',
        decimalSeparator: '',
        precision: 0);
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Цена покупки:', style: TextStyle(fontWeight: FontWeight.bold)),
      TextFormField(
          controller: controller,
          decoration: const InputDecoration(
            hintText: 'Введите себестоимость товара',
          ),
          keyboardType: TextInputType.number,
          validator: (value) {
            if (value.isEmpty) {
              return 'Поле обязательно к заполнению';
            }
            return null;
          },
          onSaved: (String value) {
            newProduct.costSelf = controller.numberValue.toInt();
          }),
    ]);
  }

  Widget radioSwitcher() {
    return Container(
      width: 400,
      child: Row(
        children: <Widget>[
          Row(children: [
            Radio(
              activeColor: Color.fromRGBO(68, 192, 192, 1),
              value: 0,
              groupValue: state,
              onChanged: (value) {
                setState(
                    () => {_animatedWidget = _costSellField(), state = value});
              },
            ),
            const Text('Продажа'),
          ]),
          Row(children: <Widget>[
            Radio(
              activeColor: Color.fromRGBO(68, 192, 192, 1),
              value: 1,
              groupValue: state,
              onChanged: (value) {
                setState(
                    () => {_animatedWidget = _writeOffField(), state = value});
              },
            ),
            const Text('Списание'),
          ])
        ],
      ),
    );
  }

  Widget switcherWidget() {
    return AnimatedSwitcher(
      duration: const Duration(seconds: 1),
      child: _animatedWidget,
    );
  }

  Widget _capacityField() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Объем упаковки (гр/мл):', style: TextStyle(fontWeight: FontWeight.bold)),
      TextFormField(
          decoration: const InputDecoration(
            hintText: 'Введите объем',
          ),
          keyboardType: TextInputType.number,
          validator: (value) {
            if (value.isEmpty) {
              return 'Поле обязательно к заполнению';
            }
            if (int.parse(value) == 0) {
              return 'Не должно быть меньше 1';
            }
            return null;
          },
          onSaved: (String value) {
            newProduct.writeOffCapacity = int.parse(value);
          }),
    ]);
  }

//виджет для продажи
  Widget _costSellField() {
    var controller = MoneyMaskedTextController(
        rightSymbol: ' ₽',
        initialValue: newProduct.costSell.toDouble(),
        thousandSeparator: ' ',
        decimalSeparator: '',
        precision: 0);
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Цена продажи:', style: TextStyle(fontWeight: FontWeight.bold)),
      TextFormField(
          controller: controller,
          decoration: const InputDecoration(
            hintText: 'Введите цену для продажи',
          ),
          keyboardType: TextInputType.number,
          validator: (value) {
            if (value.isEmpty) {
              return 'Поле обязательно к заполнению';
            }
            return null;
          },
          onSaved: (String value) {
            newProduct.costSell = controller.numberValue.toInt();
          }),
    ]);
  }

//   //Для списания виджет
  Widget _writeOffField() {
    var controller = MoneyMaskedTextController(
        rightSymbol: ' ₽',
        initialValue: newProduct.costwriteOff.toDouble(),
        thousandSeparator: ' ',
        decimalSeparator: '',
        precision: 0);
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Цена для списания (1гр):',
          style: TextStyle(fontWeight: FontWeight.bold)),
      TextFormField(
          controller: controller,
          decoration: const InputDecoration(
            hintText: 'Введите цену за 1 грамм',
          ),
          keyboardType: TextInputType.number,
          validator: (value) {
            if (value.isEmpty) {
              return 'Поле обязательно к заполнению';
            }
            return null;
          },
          onSaved: (String value) {
            newProduct.costwriteOff = controller.numberValue.toInt();
          }),
    ]);
  }
}
