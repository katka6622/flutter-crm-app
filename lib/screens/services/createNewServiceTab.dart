// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/service.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';
import 'package:validators/validators.dart';

class CreateNewServiceTab extends StatefulWidget {
  static const title = 'Добавление услуги';
  static const androidIcon = Icon(Icons.add);

  @override
  NewServiceState createState() => NewServiceState();
}

class NewServiceState extends State<CreateNewServiceTab> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  Service newService = Service();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    DatabaseHelper dbHelper = DatabaseHelper.instance;
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(CreateNewServiceTab.title),
        ),
        body: buildForm(context),
        bottomNavigationBar: Container(
            height: 50,
            margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
            child: RaisedButton(
                onPressed: () => {
                      if (_formKey.currentState.validate())
                        {
                          _formKey.currentState.save(),
                          dbHelper.insertService(newService),
                          Navigator.pop(context, 'updateState'),
                        }
                    },
                color: Color.fromRGBO(68, 192, 192, 1),
                child: new Text(
                  'СОЗДАТЬ',
                  style: TextStyle(fontSize: 18.0),
                ))));
  }

  Widget buildForm(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _nameField(),
              commonFormPadding(),
              _durationField(),
              commonFormPadding(),
              _costField(),
              commonFormPadding(),
              _commentField(),
            ],
          ),
        ));
  }

  Widget _nameField() {
    return Container(
        padding: EdgeInsets.only(top: 10.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Наименование услуги:',
                  style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0)),
              TextFormField(
                  decoration: const InputDecoration(
                      hintText: 'введите название услуги',
                      suffixStyle: TextStyle(fontSize: 10.0)),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Поле обязательно к заполнению';
                    }
                    return null;
                  },
                  onSaved: (String value) {
                    newService.name = value;
                  })
            ]));
  }

  Widget _costField() {
    var _moneyController = MoneyMaskedTextController(
        rightSymbol: ' ₽',
        initialValue: 0,
        thousandSeparator: ' ',
        decimalSeparator: '',
        precision: 0);
    return Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
          Text('Стоимость:',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0)),
          TextFormField(
              controller: _moneyController,
              //     decoration: const InputDecoration(
              //       hintText: 'Цена услуги (от)',
              //     ),
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Поле обязательно к заполнению';
                }
                return null;
              },
              onSaved: (String value) {
                newService.cost = _moneyController.numberValue.toInt();
              })
        ]));
  }

  Widget _durationField() {
    var controller =
        TextEditingController(text: formatTime(newService.duration ?? 0));
    Widget upperTextWidget = Text('Продолжительность:',
        style: TextStyle(fontWeight: FontWeight.bold));
    var callback = (DateTime time) {
      setState(() {
        newService.duration = 60 * time.hour + time.minute;
        controller.text = newService.duration.toString();
      });
    };
    return popupTimePickerField(
        context, controller, callback, upperTextWidget, 'Сохранить');
  }

  Widget _commentField() {
    return Container(
        padding: EdgeInsets.only(top: 10.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Комментарий:',
                  style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0)),
              TextFormField(
                // decoration: const InputDecoration(
                //   hintText: 'Комментарий',
                // ),
                onSaved: (String value) {
                  newService.comment = value;
                },
              )
            ]));
  }
}
