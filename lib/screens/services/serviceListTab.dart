// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';

import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/drawer.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/service.dart';
import 'package:flutter_crm_app/screens/services/createNewServiceTab.dart';
import 'package:flutter_crm_app/screens/services/editServiceTab.dart';

class ServiceListTab extends StatefulWidget {
  static const title = 'Услуги';
  static const androidIcon = Icon(Icons.featured_play_list);

  @override
  ServiceListState createState() => ServiceListState();
}

class ServiceListState extends State<ServiceListTab> {
  final _scaffoldMainKey = GlobalKey<ScaffoldState>();
  Future<List<Service>> serviceList;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldMainKey,
        appBar: AppBar(
          title: Text('Услуги'),
          actions: <Widget>[],
        ),
        drawer: AndroidDrawer(), //call
        body: buildServiceList(context),
        floatingActionButton: Theme(
          data: Theme.of(context).copyWith(
            colorScheme: Theme.of(context)
                .colorScheme
                .copyWith(secondary: Color.fromRGBO(68, 192, 192, 1)),
          ),
          child: FloatingActionButton(
            onPressed: () {
              _navigateToCreate(context, _scaffoldMainKey);
            },
            child: Icon(Icons.add),
          ),
        ));
  }

  Future<List<Service>> _loadData(DatabaseHelper helper) async {
    return await helper.serviceList();
  }

  Widget buildServiceList(BuildContext context) {
    DatabaseHelper helper = DatabaseHelper.instance;
    serviceList = _loadData(helper);
    return FutureBuilder<List<Service>>(
        future: serviceList,
        builder: (context, snapshot) {
          List<Service> serviceList = snapshot.data ?? [];
          return ListView.builder(
              padding: const EdgeInsets.only(top: 10.0),
              itemCount: serviceList.length,
              itemBuilder: (context, index) {
                Service service = serviceList[index];
                return _buildRow(service);
              });
        });
  }

  Widget _buildRow(Service service) {
    return ListTile(
        title: _buildServiceRow(service),
        onTap: () {
          _navigateToEdit(context, service);
        });
  }

  Widget _buildServiceRow(Service service) {
    return Container(
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: Colors.grey[200])),
      ),
      width: double.infinity,
      height: 60,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(
                service.name,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    formatMoney(service.cost).toString(),
                    style: TextStyle(fontSize: 14.0),
                  ),
                  Padding(padding: EdgeInsets.symmetric(horizontal: 4.0)),
                  Text(
                    formatTime(service.duration),
                    style: TextStyle(fontSize: 14.0),
                  ),
                ],
              ),
            ],
          ),
          Icon(Icons.more_vert, color: Color.fromRGBO(68, 192, 192, 1)),
        ],
      ),
    );
  }

  _navigateToCreate(BuildContext context, _scaffoldMainKey) async {
    var createResult = await Navigator.push(_scaffoldMainKey.currentContext,
        MaterialPageRoute(builder: (context) => new CreateNewServiceTab()));

    if (createResult == 'updateState') {
      setState(() {});
    }
  }

  _navigateToEdit(BuildContext context, Service service) async {
    var updatedStatus = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => EditServiceTab(service: service)),
    );
    if (updatedStatus == 'updateState') {
      setState(() {});
    }
  }
}
