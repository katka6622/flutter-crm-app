// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:validators/validators.dart';
import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/service.dart';

class EditServiceTab extends StatefulWidget {
  static const title = 'Изменение услуги';
  static const androidIcon = Icon(Icons.list);
  final Service service;

  const EditServiceTab({Key key, this.service}) : super(key: key);

  @override
  EditServiceState createState() => EditServiceState();
}

class EditServiceState extends State<EditServiceTab> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  DatabaseHelper dbHelper = DatabaseHelper.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(EditServiceTab.title),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.delete_outline,
                color: Colors.red[400],
              ),
              onPressed: () => {
                _formKey.currentState.save(),
                dbHelper.removeService(widget.service.id),
                Navigator.pop(context, 'updateState'),
              },
            ),
          ],
        ),
        body: buildForm(context),
        bottomNavigationBar: Container(
            height: 50,
            margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
            child: RaisedButton(
                onPressed: () => {
                      if (_formKey.currentState.validate())
                        {
                          _scaffoldKey.currentState.showSnackBar(
                            SnackBar(
                              content: Text('Услуга сохранёна'),
                              duration: Duration(seconds: 1),
                            ),
                          ),
                          _formKey.currentState.save(),
                          dbHelper.updateService(widget.service),
                          Navigator.pop(context, 'updateState'),
                        }
                    },
                color: Color.fromRGBO(68, 192, 192, 1),
                child: new Text(
                  'СОХРАНИТЬ',
                  style: TextStyle(fontSize: 18.0),
                ))));
  }

  final _formKey = GlobalKey<FormState>();

  Widget buildForm(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _nameField(),
              commonFormPadding(),
              _costField(),
              commonFormPadding(),
              _durationField(),
              commonFormPadding(),
              _commentField(),
            ],
          ),
        ));
  }

  Widget _nameField() {
    return Container(
        padding: EdgeInsets.only(top: 10.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Наименование услуги:',
                  style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0)),
              TextFormField(
                  initialValue: widget.service.name,
                  decoration: const InputDecoration(
                    hintText: 'Название услуги',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Поле обязательно к заполнению';
                    }
                    return null;
                  },
                  onSaved: (String value) {
                    widget.service.name = value;
                  })
            ]));
  }

  Widget _costField() {
    var moneyController = MoneyMaskedTextController(
        rightSymbol: ' ₽',
        initialValue: widget.service.cost.toDouble(),
        thousandSeparator: ' ',
        decimalSeparator: '',
        precision: 0);
    return Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
          Text('Стоимость (от):',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0)),
          TextFormField(
              controller: moneyController,
              decoration: const InputDecoration(
                hintText: 'Цена услуги (от)',
              ),
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Поле обязательно к заполнению';
                }
                return null;
              },
              onSaved: (String value) {
                widget.service.cost = moneyController.numberValue.toInt();
              })
        ]));
  }

  Widget _durationField() {
    var controller =
        TextEditingController(text: formatTime(widget.service.duration));
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Продолжительность:',
              style: TextStyle(fontWeight: FontWeight.bold)),
          TextFormField(
            readOnly: true,
            controller: controller,
            onTap: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                        content: timepicker(
                          (DateTime time) {
                            setState(() {
                              widget.service.duration =
                                  60 * time.hour + time.minute;
                              controller.text =
                                  widget.service.duration.toString();
                            });
                          },
                        ),
                        actions: [
                          FlatButton(
                            child: Text('Сохранить'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ]);
                  });
            },
          )
        ]);
  }

  Widget _commentField() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Комментарий:', style: TextStyle(fontWeight: FontWeight.bold)),
          TextFormField(
              initialValue: widget.service.comment,
              maxLines: 3,
              decoration: const InputDecoration(
                hintText: 'Комментарий',
              ),
              onSaved: (String value) {
                widget.service.comment = value;
              })
        ]);
  }
}
