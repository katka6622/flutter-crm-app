import 'package:flutter/material.dart';
import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/consumption.dart';

class CreateNewConsumptionType extends StatefulWidget {
  static const title = 'Добавление вида расходов';
  static const androidIcon = Icon(Icons.local_mall);

  @override
  CreateNewConsumptionTypeState createState() => CreateNewConsumptionTypeState();
}

class CreateNewConsumptionTypeState extends State<CreateNewConsumptionType> {
  DatabaseHelper helper = DatabaseHelper.instance;
  ConsumptionType newConsumptionType = new ConsumptionType();

  @override
  Widget build(BuildContext context) {
    DatabaseHelper dbHelper = DatabaseHelper.instance;
    var _scaffoldKey;
        return Scaffold(
            key: _scaffoldKey,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          title: Text(CreateNewConsumptionType.title),
        ),
        body: buildForm(context),
        bottomNavigationBar: Container(
            height: 50,
            margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
            child: RaisedButton(
                onPressed: () => {
                      if (_formKey.currentState.validate())
                        {
                          _formKey.currentState.save(),
                          dbHelper.insertConsumptionType(newConsumptionType),
                          Navigator.pop(context, 'updateState'),
                        }
                    },
                color: Color.fromRGBO(68, 192, 192, 1),
                child: new Text(
                  'СОЗДАТЬ',
                  style: TextStyle(fontSize: 18.0),
                ))));
  }

  final _formKey = GlobalKey<FormState>();

  Widget buildForm(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _nameField(),
              commonFormPadding(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _nameField() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Наименование вида расхода:', style: TextStyle(fontWeight: FontWeight.bold)),
      TextFormField(
          decoration: const InputDecoration(
            hintText: 'Введите вид расхода',
          ),
          validator: (value) {
            if (value.isEmpty) {
              return 'Поле обязательно к заполнению';
            }
            return null;
          },
          onSaved: (String value) {
            newConsumptionType.name = value;
          }),
    ]);
  }
}