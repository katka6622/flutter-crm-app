import 'package:flutter/material.dart';
import 'package:flutter_crm_app/drawer.dart';
import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/consumption.dart';

import 'createNewConsumptionType.dart';

class ConsymptionTypeTab extends StatefulWidget {
  static const title = 'Виды расходов';

  @override
  ConsymptionTypeState createState() => ConsymptionTypeState();
}

class ConsymptionTypeState extends State<ConsymptionTypeTab> {
  final _scaffoldMainKey = GlobalKey<ScaffoldState>();
  DatabaseHelper dbHelper = DatabaseHelper.instance;
  Future<List<ConsumptionType>> consumptionTypeList;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldMainKey,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(ConsymptionTypeTab.title),
        actions: <Widget>[],
      ),
      body: _buildTypeList(),
      floatingActionButton: Theme(
        data: Theme.of(context).copyWith(
          colorScheme: Theme.of(context)
              .colorScheme
              .copyWith(secondary: Color.fromRGBO(68, 192, 192, 1)),
        ),
        child: FloatingActionButton(
          onPressed: () {
            _navigateToCreate(context, _scaffoldMainKey);
          },
          child: Icon(Icons.add),
        ),
      ),
    );
  }

  Widget _buildTypeList() {
    return FutureBuilder<List<ConsumptionType>>(
        future: dbHelper.consumptionTypeList(),
        builder: (context, snapshot) {
          List<ConsumptionType> typeList = snapshot.data ?? [];
          return ListView.builder(
              padding: const EdgeInsets.all(0),
              itemCount: typeList.length,
              itemBuilder: (context, index) {
                ConsumptionType type = typeList[index];
                return buildTypeRow(type);
              });
        });
  }

  Widget buildTypeRow(ConsumptionType type)  {
    return Container(
      decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(color: Color.fromRGBO(68, 192, 192, 0.2))),
            color: Color.fromRGBO(68, 192, 192, 0.1)),
        width: double.infinity,
        padding: EdgeInsets.all(20.0),
        child: Text(type.name, style: TextStyle(fontSize: 18.0)));
  }

  _navigateToCreate(BuildContext context, _scaffoldMainKey) async {
    var createResult = await Navigator.push(
        _scaffoldMainKey.currentContext,
        MaterialPageRoute(
            builder: (context) => new CreateNewConsumptionType()));

    if (createResult == 'updateState') {
      setState(() {});
    }
  }
}
