import 'package:flutter/material.dart';
import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/drawer.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/consumption.dart';
import 'package:flutter_crm_app/screens/setting/consumptionType.dart';

class SettingsTab extends StatefulWidget {
  static const title = 'Настройки';
  static const androidIcon = Icon(Icons.settings);

  @override
  SettingsState createState() => SettingsState();
}

class SettingsState extends State<SettingsTab> {
  final _scaffoldMainKey = GlobalKey<ScaffoldState>();
  DatabaseHelper dbHelper = DatabaseHelper.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldMainKey,
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text(SettingsTab.title),
        actions: <Widget>[],
      ),
      drawer: AndroidDrawer(), //call
      body: buildTraffic(context),
    );
  }

  final _formKey = GlobalKey<FormState>();

  Widget buildTraffic(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _consumptionButton(),
              commonFormPadding(),
              
              
            ]),
      ),
    );
  }

  Widget _consumptionButton() {
    return Center(
      child: RaisedButton(
        onPressed: () {
          _navigateToTab(context);
        },
        child: Text(
          'Виды расходов',
          style: TextStyle(fontSize: 16.0),
        ),
        color: Color.fromRGBO(68, 192, 192, 1),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5))),
        
      ),
    );
  }

  _navigateToTab(BuildContext context) async {
    await Navigator.push(_scaffoldMainKey.currentContext,
        MaterialPageRoute(builder: (context) => new ConsymptionTypeTab()));
  }
}