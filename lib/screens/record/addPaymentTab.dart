// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter_crm_app/models/baseModel.dart';
import 'package:flutter_crm_app/models/clientRecordModel.dart';
import 'package:flutter_crm_app/models/employee.dart';
import 'package:flutter_crm_app/models/finance.dart';
import 'package:flutter_crm_app/models/service.dart';
import 'package:flutter_crm_app/models/clientRecord.dart';
import 'package:flutter_crm_app/models/client.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:intl/intl.dart';

import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:table_calendar/table_calendar.dart';

import 'package:validators/validators.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class AddPaymentTab extends StatefulWidget {
  static const title = 'Оплата записи';
  static const androidIcon = Icon(Icons.add);
  final ClientRecord record;

  const AddPaymentTab({Key key, this.record}) : super(key: key);

  @override
  AddPaymentState createState() => AddPaymentState();
}

class AddPaymentState extends State<AddPaymentTab> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  DatabaseHelper dbHelper = DatabaseHelper.instance;
  ClientRecordModel clientRecord;
  List<Service> selectedServices = [];
  List<int> selectedServicesIds = [];
  int totalCost = 0;

  @override
  void initState() {
    super.initState();
    clientRecord = widget.record;
    getClientRecordSelectedServiceIds();
  }

  getClientRecordSelectedServiceIds() async {
    selectedServicesIds = await dbHelper.getClientRecordServiceIds(clientRecord);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text(AddPaymentTab.title),
        ),
        body: buildForm(context),
        bottomNavigationBar: Container(
            height: 50,
            margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
            child: RaisedButton(
                onPressed: validateForm,
                color: Color.fromRGBO(68, 192, 192, 1),
                child: new Text(
                  'СОХРАНИТЬ',
                  style: TextStyle(fontSize: 18.0),
                ))));
  }

  validateForm() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      Finance newPayment = await dbHelper.addPaymentForService(totalCost, clientRecord);
      Navigator.pop(context, {'newPayment': newPayment, 'status': 'success'});
    }
  }

  final _formKey = GlobalKey<FormState>();

  Widget buildForm(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              commonFormPadding(),
              _servicesField(),
              commonFormPadding(),
              _costField(),
            ],
          ),
        ));
  }

  Widget _servicesField() {
    var validator = (selectedItems) {
      if (selectedItems == null || selectedItems.isEmpty) {
        return ("Выберите услугу");
      }
      return null;
    };
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Услуги', style: TextStyle(fontWeight: FontWeight.bold),),
        multiSelectServicesField(dbHelper.serviceList(), "Выберите услуги",
            "Выберите одну или несколько услуг", validator),
      ],
    );
  }

  Widget _costField() {
    var moneyController = MoneyMaskedTextController(
        rightSymbol: ' ₽',
        initialValue: totalCost.toDouble(),
        thousandSeparator: ' ',
        decimalSeparator: '',
        precision: 0);
    return Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
          Text('Сумма:',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0)),
          TextFormField(
              controller: moneyController,
              decoration: const InputDecoration(
                hintText: 'Сумма:',
              ),
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Поле обязательно к заполнению';
                }
                if (moneyController.numberValue.toInt() <= 0)  {
                  return 'Введите сумму оплаты';
                }
                return null;
              },
              onSaved: (String value) {
                totalCost = moneyController.numberValue.toInt();
              })
        ]));
  }

  // GOVNOKOD
  Widget multiSelectServicesField(
      futureList, hintText, searchHintText, validator) {
    return FutureBuilder(
        future: futureList,
        builder: (context, snapshot) {
          List items = _convertToDropdown(snapshot.data) ?? [];
          //govnokod
          Map selectedValues = items.asMap();
          return SearchableDropdown.multiple(
            key: new Key('selectServices'),
            items: items,
            selectedItems: selectedServicesIds,
            hint: Padding(
              padding: const EdgeInsets.fromLTRB(0,5,0,5),
              child: Text(hintText),
            ),
            searchHint: searchHintText,
            validator: validator,
            onChanged: (value) {
              setState(() {
                if (value != null) {
                  value.forEach((index) {
                    selectedServices.add(selectedValues[index].value);
                  });
                  selectedServicesIds = value;
                }
              });
            },
            closeButton: "Сохранить",
            isExpanded: true,
          );
        });
  }

  
  List<DropdownMenuItem<BaseModel>> _convertToDropdown(List dataList) {
    List<DropdownMenuItem<BaseModel>> result = [];
    if (dataList != null) {
      dataList.forEach((element) {
        DropdownMenuItem<BaseModel> newElem =
            new DropdownMenuItem(value: element, child: Text(element.name));
        result.add(newElem);
      });
    }
    return result;
  }
}
