// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';

import 'package:flutter_crm_app/drawer.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/clientRecord.dart';
import 'package:flutter_crm_app/models/clientRecordModel.dart';
import 'package:flutter_crm_app/models/employee.dart';
import 'package:flutter_crm_app/screens/record/createNewRecordTab.dart';
import 'package:flutter_crm_app/screens/record/editRecordTab.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';

import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_week_view/flutter_week_view.dart';
import 'dart:ui';

const hourHeight = 80.0;
const int workingHours = 15;
const int startHour = 9;
const int gridHeigth = 90;
const int paddingFault = 100;

class RecordListTab extends StatefulWidget {
  static const title = 'Журнал записей';
  static const androidIcon = Icon(Icons.import_contacts);

  @override
  RecordListState createState() => RecordListState();
}

class RecordListState extends State<RecordListTab>
    with TickerProviderStateMixin {
  final _scaffoldMainKey = GlobalKey<ScaffoldState>();
  List _selectedEvents;
  AnimationController _animationController;
  CalendarController _calendarController;
  DatabaseHelper dbHelper = DatabaseHelper.instance;
  Future<Map<DateTime, List<ClientRecordModel>>> recordList;
  List<Employee> workingMasterList = [];
  Map<DateTime, List<ClientRecordModel>> eventList;
  DateTime visibleDateFrom;
  DateTime visibleDateTo;
  DateTime selectedDay = DateTime.now();

  @override
  void initState() {
    super.initState();

    _selectedEvents = [];
    workingMasterList = [];
    _calendarController = CalendarController();

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    _animationController.forward();
    WidgetsBinding.instance.addPostFrameCallback(
        (_) => _updateGreedState(visibleDateFrom, visibleDateTo));
  }

  @override
  void dispose() {
    _animationController.dispose();
    _calendarController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldMainKey,
      appBar: AppBar(
        title: Text('Журнал записей'),
        actions: <Widget>[],
      ),
      drawer: AndroidDrawer(),
      body: FutureBuilder(
        future: recordList,
        builder: (context, snapshot) {
          Map<DateTime, List<ClientRecordModel>> records = snapshot.data ?? {};
          return Column(
            children: [
              buildTableCalendar(records),
              Expanded(child: customDayView(_selectedEvents)),
            ],
          );
        },
      ),
      floatingActionButton: Theme(
        data: Theme.of(context).copyWith(
          colorScheme: Theme.of(context)
              .colorScheme
              .copyWith(secondary: Color.fromRGBO(68, 192, 192, 1)),
        ),
        child: FloatingActionButton(
          onPressed: () {
            _navigateToCreate(context, _scaffoldMainKey);
          },
          child: Icon(Icons.add),
        ),
      ),
    );
  }

  Future getWorkingMasters(DateTime date) async {
    var list = await dbHelper.getWorkingEmployeeList(date);
    setState(() {
      workingMasterList = list;
    });
  }

  Future<Map<DateTime, List<ClientRecordModel>>> getRecordList(
      DateTime dateFrom, DateTime dateTo) async {
    DateFormat formatter = DateFormat('yyyy-MM-dd');
    String dateFromValue =
        new DateTime(dateFrom.year, dateFrom.month, dateFrom.day, 0, 0)
            .toIso8601String();
    String dateToValue =
        new DateTime(dateTo.year, dateTo.month, dateTo.day, 23, 59, 59)
            .toIso8601String();
    var list = await dbHelper.clientRecordList(dateFromValue, dateToValue);
    Map<DateTime, List<ClientRecordModel>> res = {};
    list.forEach((ClientRecordModel element) {
      DateTime dateKey = DateTime.parse(formatter.format(element.recordDate));
      if (res.containsKey(dateKey)) {
        res[dateKey].add(element);
      } else {
        res[dateKey] = [element];
      }
    });
    return res;
  }

  Widget buildTableCalendar(records) {
    var onDaySelectedCallback = (date, events) {
      _onDaySelected(date, events);
      _animationController.forward(from: 0.0);
    };
    return recordCalendar(_calendarController, _animationController, records,
        onDaySelectedCallback, _onVisibleDaysChanged, _onCalendarCreated);
  }

  Widget customDayView(List todayEvents) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverToBoxAdapter(
          child: _buildDayGreed(todayEvents),
        )
      ],
    );
  }

  Widget _buildDayGreed(List todayEvents) {
    double calendarHeight =
        (workingHours * gridHeigth - paddingFault).toDouble();
    int masterCount = 0;
    List<Widget> resultGrid = [];
    List<Widget> masterGrid = [];
    int gridWidth = 200;
    Map<int, List> masterIdList = {};

    todayEvents.forEach((element) {
      if (masterIdList.containsKey(element.masterId)) {
        masterIdList[element.masterId].add(element);
      } else {
        masterIdList[element.masterId] = [element];
      }
    });

    workingMasterList.forEach((element) {
      masterGrid.add(buildTopMastersGrid(element.name));
    });
    masterCount = workingMasterList.length;
    gridWidth = masterCount * 201;
    workingMasterList.forEach((master) {
      List<Widget> masterRecords = [];
      masterIdList.forEach((key, List value) {
        value.forEach((element) {
          int minuteCost = (element.recordDate.minute == 30) ? 1 : 0;
          int greedTimeCost = (element.recordDate.hour - startHour) * 2 + minuteCost;
          if (master.id == element.masterId) {
            masterRecords.add(_buildClientRecordElement(greedTimeCost,
                clientRecord: element));
          }
        });
      });
      resultGrid.add(Container(
        width: 200,
        margin: EdgeInsets.only(right: 1),
        child: Column(
          children: [
            Expanded(
              child: Stack(children: masterRecords),
            )
          ],
        ),
      ));
    });

    return Container(
      width: (masterCount * 20).toDouble(),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Container(
          child: Row(
            //All calendar
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildLeftTimeGrid(workingHours),
              Container(
                //Main Calendar
                height: calendarHeight,
                width: gridWidth.toDouble(),
                child: Column(
                  children: <Widget>[
                    Row(
                      textBaseline: TextBaseline.ideographic,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: masterGrid,
                    ),
                    Expanded(
                      child: SizedBox(
                        // RECORDS
                        width: gridWidth.toDouble(),
                        child: Row(
                          children: resultGrid,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildClientRecordElement(int d, {ClientRecordModel clientRecord}) {
    double halfHourHeight = hourHeight / 2;
    double recordHeight = halfHourHeight * (clientRecord.recordDuration ~/ 30);
    DateTime _endTime = clientRecord.recordDate
        .add(new Duration(minutes: clientRecord.recordDuration));
    return Positioned(
      top: (d * halfHourHeight).toDouble(),
      child: GestureDetector(
        onTap: () => {_navigateToEdit(clientRecord)},
        child: masterRecord(
          recordHeight,
          clientRecord,
          clientRecord.recordDate,
          _endTime,
        ),
      ),
    );
  }

  Widget recordCalendar(
      CalendarController calendarController,
      AnimationController _animationController,
      events,
      onDaySelected,
      onVisibleDaysChanged,
      onCalendarCreated) {
    return TableCalendar(
      locale: 'ru_RU',
      calendarController: calendarController,
      events: events,
      initialCalendarFormat: CalendarFormat.week,
      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.monday,
      availableGestures: AvailableGestures.all,
      availableCalendarFormats: const {
        CalendarFormat.month: 'Неделя',
        CalendarFormat.week: 'Месяц',
      },
      calendarStyle: CalendarStyle(
        outsideDaysVisible: false,
        weekendStyle: TextStyle().copyWith(color: Colors.red[300]),
      ),
      daysOfWeekStyle: DaysOfWeekStyle(
        weekendStyle: TextStyle().copyWith(color: Colors.red[300]),
      ),
      headerStyle: HeaderStyle(
        centerHeaderTitle: true,
        formatButtonVisible: true,
      ),
      builders: CalendarBuilders(
        selectedDayBuilder: (context, date, _) {
          return FadeTransition(
            opacity: Tween(begin: 0.0, end: 1.0).animate(_animationController),
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.deepOrange[300],
              ),
              margin: const EdgeInsets.all(4.0),
              padding: const EdgeInsets.only(top: 15.0, left: 15.0),
              width: 100,
              height: 100,
              child: Text(
                '${date.day}',
                style: TextStyle().copyWith(fontSize: 16.0),
              ),
            ),
          );
        },
        todayDayBuilder: (context, date, _) {
          return Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.amber[400],
            ),
            margin: const EdgeInsets.all(4.0),
            padding: const EdgeInsets.only(top: 15.0, left: 15.0),
            width: 100,
            height: 100,
            child: Text(
              '${date.day}',
              style: TextStyle().copyWith(fontSize: 16.0),
            ),
          );
        },
        markersBuilder: (context, date, events, holidays) {
          final children = <Widget>[];
          if (events.isNotEmpty) {
            children.add(
              Positioned(
                right: 1,
                bottom: 1,
                child: _buildCalendarEventsMarker(
                    date, events, calendarController),
              ),
            );
          }

          return children;
        },
      ),
      onDaySelected: onDaySelected,
      onVisibleDaysChanged: onVisibleDaysChanged,
      onCalendarCreated: onCalendarCreated,
    );
  }

  Widget _buildCalendarEventsMarker(
      DateTime date, List events, CalendarController calendarController) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: calendarController.isSelected(date)
            ? Colors.brown[500]
            : calendarController.isToday(date)
                ? Colors.brown[300]
                : Colors.blue[400],
      ),
      width: 30.0,
      height: 8.0,
      margin: EdgeInsets.only(bottom: 12),
      child: Center(
        child: Text(
          '',
          style: TextStyle().copyWith(
            color: Colors.white,
            fontSize: 10.0,
          ),
        ),
      ),
    );
  }

  Widget _buildLeftTimeGrid(int workingHours) {
    var _startTime = HourMinute(hour: startHour, minute: 0);
    List<Widget> timeGrid = [
      Container(
        height: 70,
        width: 60,
        color: Color.fromRGBO(68, 192, 192, 1),
      )
    ];
    double width = 60;
    timeGrid.addAll(List.generate(workingHours,
        (index) => leftOneHourContainer(width, hourHeight, index, _startTime)));
    return Container(
      child: Column(
        children: timeGrid,
      ),
    );
  }

  void _onDaySelected(DateTime day, List events) {
    selectedDay = day;
    getWorkingMasters(selectedDay);
    setState(() {
      _selectedEvents = events;
      recordList = getRecordList(visibleDateFrom, visibleDateTo);
    });
  }

  void _onVisibleDaysChanged(
      DateTime dateFrom, DateTime dateTo, CalendarFormat format) async {
    visibleDateFrom = dateFrom;
    visibleDateTo = dateTo;
    _updateGreedState(visibleDateFrom, visibleDateTo);
  }

  void _onCalendarCreated(
      DateTime dateFrom, DateTime dateTo, CalendarFormat format) async {
    visibleDateFrom = dateFrom;
    visibleDateTo = dateTo;
  }

  _navigateToCreate(BuildContext context, _scaffoldMainKey) async {
    var createResult = await Navigator.push(
        _scaffoldMainKey.currentContext,
        MaterialPageRoute(
            builder: (context) =>
                new CreateNewRecordTab(selectedDay: selectedDay)));
    if (createResult == 'updateState') {
      _updateGreedState(visibleDateFrom, visibleDateTo);
    }
  }

  _updateGreedState(DateTime visibleDateFrom, DateTime visibleDateTo) {
    setState(() {
      recordList = getRecordList(visibleDateFrom, visibleDateTo);
    });
  }

  _navigateToEdit(ClientRecord model) async {
    var createResult = await Navigator.push(
        _scaffoldMainKey.currentContext,
        MaterialPageRoute(
            builder: (context) => new EditRecordTab(record: model)));
    if (createResult == 'updateState') {
      _selectedEvents.remove(model);
      _updateGreedState(visibleDateFrom, visibleDateTo);
    }
  }
}
