// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter_crm_app/models/baseModel.dart';
import 'package:flutter_crm_app/models/employee.dart';
import 'package:flutter_crm_app/models/service.dart';
import 'package:flutter_crm_app/models/clientRecord.dart';
import 'package:flutter_crm_app/models/client.dart';
import 'package:intl/intl.dart';

import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

import 'package:validators/validators.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class CreateNewRecordTab extends StatefulWidget {
  static const title = 'Новая запись';
  static const androidIcon = Icon(Icons.add);
  final DateTime selectedDay;

  const CreateNewRecordTab({Key key, this.selectedDay}) : super(key: key);

  @override
  NewRecordState createState() => NewRecordState();
}

class NewRecordState extends State<CreateNewRecordTab> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  ClientRecord newRecord = ClientRecord();
  DatabaseHelper dbHelper = DatabaseHelper.instance;
  Client selectedClient;
  Employee selectedMaster;
  DateTime selectedTime = new DateTime(2020, 01, 01, 13, 30);
  List<Service> selectedServices = [];
  List<int> selectedServicesIds = [];
  bool masterAllowed = true;
  bool masterNotWork = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text(CreateNewRecordTab.title),
        ),
        body: buildForm(context),
        bottomNavigationBar: Container(
            height: 50,
            margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
            child: RaisedButton(
                onPressed: validateForm,
                color: Color.fromRGBO(68, 192, 192, 1),
                child: new Text(
                  'СОЗДАТЬ',
                  style: TextStyle(fontSize: 18.0),
                ))));
  }

  validateForm() async {
    Map validateResult;
    masterAllowed = true;
    masterNotWork = false;
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      newRecord.masterId = selectedMaster.id;
      newRecord.clientId = selectedClient.id;
      newRecord.recordDate = new DateTime(
        newRecord.recordDate.year,
        newRecord.recordDate.month,
        newRecord.recordDate.day,
        selectedTime.hour,
        selectedTime.minute,
      );

      validateResult =
          await dbHelper.createNewClientRecord(newRecord, selectedServices);
      if (validateResult['result'] == 'errorNotWork') {
        masterNotWork = true;
        _formKey.currentState.validate();
      } else {
        if (validateResult['result'] == 'error') {
          masterAllowed = false;
          _formKey.currentState.validate();
        } else {
          Navigator.pop(context, 'updateState');
        }
      }
    }
  }

  final _formKey = GlobalKey<FormState>();

  Widget buildForm(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  
                  children: <Widget>[
                    Expanded(child: _recordDateField(context)),
                    Padding(padding: EdgeInsets.only(left: 20)),
                    Expanded(child: _recordTimeField()),
                  ],
                ),
              ),
              commonFormPadding(),
              _clientField(),
              commonFormPadding(),
              _masterField(),
              commonFormPadding(),
              _servicesField(),
              commonFormPadding()
            ],
          ),
        ));
  }

  List<DropdownMenuItem<BaseModel>> _convertToDropdown(List dataList) {
    List<DropdownMenuItem<BaseModel>> result = [];
    if (dataList != null) {
      dataList.forEach((element) {
        DropdownMenuItem<BaseModel> newElem =
            new DropdownMenuItem(value: element, child: Text(element.name));
        result.add(newElem);
      });
    }
    return result;
  }

  List<int> searchFn(keyword, List items) {
    List<int> res = [];
    if (items != null) {
      items.asMap().forEach((index, element) {
        if (element.value.name.toLowerCase().contains(keyword) ||
            element.value.phone.contains(keyword)) {
          res.add(index);
        }
      });
    }

    return res;
  }

  Widget _clientField() {
    var callback = (value) {
      setState(() {
        selectedClient = value;
      });
    };
    var validator = (selectedItems) {
      if (selectedItems == null) {
        return ("Выберите клиента");
      }
      return null;
    };
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Клиент', style: TextStyle(fontWeight: FontWeight.bold),),
        searchPopupField(
            dbHelper.clients(),
            "Выберите клиента",
            "Введите телефон или имя клиента",
            selectedClient,
            callback,
            validator)
      ],
    );
  }

  Widget _masterField() {
    var callback = (value) {
      setState(() {
        selectedMaster = value;
      });
    };
    var validator = (value) {
      if (value == null) {
        return ("Выберите Мастера");
      }
      return null;
    };
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Мастер', style: TextStyle(fontWeight: FontWeight.bold),),
        searchPopupField(
            dbHelper.employeeList(),
            "Выберите мастера",
            "Выберите мастера",
            selectedMaster,
            callback,
            validator),
      ],
    );
  }

  Widget _servicesField() {
    var validator = (selectedItems) {
      if (selectedItems == null || selectedItems.isEmpty) {
        return ("Выберите услугу");
      }
      return null;
    };
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Услуги', style: TextStyle(fontWeight: FontWeight.bold),),
        multiSelectServicesField(dbHelper.serviceList(), "Выберите услуги",
            "Выберите одну или несколько услуг", validator),
      ],
    );
  }

  Widget _recordDateField(context) {
    final format = DateFormat("dd-MM-yyyy");
    return Column(mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text('Дата новой записи', style: TextStyle(fontWeight: FontWeight.bold),),
      DateTimeField(
        format: format,
        initialValue: widget.selectedDay,
        onShowPicker: (context2, currentValue) {
          return showDatePicker(
              context: context,
              firstDate: DateTime(2020),
              lastDate: DateTime(2100),
              initialDate: currentValue ?? widget.selectedDay,
              helpText: 'Выбери дату записи'
              // locale: Locale.fromSubtags(languageCode: 'RU'),
              );
        },
        validator: (value) {
          if (value == null) {
            return ("Выберите дату записи");
          }
          if (!masterAllowed) {
            return 'Мастер занят';
          }
          if (masterNotWork) {
            return 'Мастер не работает';
          }
        },
        onSaved: (DateTime date) {
          newRecord.recordDate = date;
        },
      ),
    ]);
  }

  Widget _recordTimeField() {
    var controller =
        TextEditingController(text: formatTimeOutput(selectedTime));
    Widget upperTextWidget =
        Text('Время записи:', style: TextStyle(fontWeight: FontWeight.bold));
    var callback = (DateTime time) {
      setState(() {
        selectedTime = time;
        controller.text = selectedTime.toString();
        // controller.text = time.hour.toString() + ':' + time.minute.toString();
      });
    };
    return popupTimePickerField(
        context, controller, callback, upperTextWidget, 'Сохранить',
        initialTime: selectedTime);
  }

  // GOVNOKOD
  Widget multiSelectServicesField(
      futureList, hintText, searchHintText, validator) {
    return FutureBuilder(
        future: futureList,
        builder: (context, snapshot) {
          List items = _convertToDropdown(snapshot.data) ?? [];
          //govnokod
          Map selectedValues = items.asMap();
          return SearchableDropdown.multiple(
            key: new Key('selectServices'),
            items: items,
            selectedItems: selectedServicesIds,
            hint: Padding(
              padding: const EdgeInsets.fromLTRB(0,5,0,5),
              child: Text(hintText),
            ),
            searchHint: searchHintText,
            validator: validator,
            onChanged: (value) {
              setState(() {
                if (value != null) {
                  value.forEach((index) {
                    selectedServices.add(selectedValues[index].value);
                  });
                  selectedServicesIds = value;
                }
              });
            },
            closeButton: "Сохранить",
            isExpanded: true,
          );
        });
  }
}
