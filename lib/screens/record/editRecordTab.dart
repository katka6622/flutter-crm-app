// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter_crm_app/models/clientRecordModel.dart';
import 'package:flutter_crm_app/models/clientRecord.dart';
import 'package:flutter_crm_app/models/finance.dart';
import 'package:flutter_crm_app/screens/record/addPaymentTab.dart';
import 'package:intl/intl.dart';

import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class EditRecordTab extends StatefulWidget {
  static const title = 'Детали записи';
  static const androidIcon = Icon(Icons.add);
  final ClientRecord record;

  const EditRecordTab({Key key, this.record}) : super(key: key);

  @override
  RecordState createState() => RecordState();
}

class RecordState extends State<EditRecordTab> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  DatabaseHelper dbHelper = DatabaseHelper.instance;
  DateTime newRecordTime = new DateTime(2020, 01, 01, 12, 00);
  DateTime newRecordDate = DateTime.now();
  bool masterAllowed = true;
  ClientRecordModel clientRecord;
  List<Finance> paymentList = [];
  String _paymentStatus = ClientRecord.nonPaidStatus;
  bool _paymentBtnEnabled = true;

  getRecordDataFromDb() async {
    clientRecord = await dbHelper.getClientRecordInfoById(widget.record);

    if (clientRecord.recordStatus == ClientRecord.paidStatus) {
      setState(() {
        _paymentStatus = clientRecord.recordStatus;
        _paymentBtnEnabled =
            clientRecord.recordStatus != ClientRecord.paidStatus;
      });
    }
    return clientRecord;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text(EditRecordTab.title),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.delete_outline,
                color: Colors.red[400],
              ),
              onPressed: () => {
                _formKey.currentState.save(),
                dbHelper.removeClientRecord(clientRecord),
                Navigator.pop(context, 'updateState'),
              },
            ),
          ],
        ),
        body: FutureBuilder(
          future: getRecordDataFromDb(),
          builder: (context, snapshot) {
            return buildForm(context);
          },
        ),
        bottomNavigationBar: Container(
            height: 50,
            margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
            child: RaisedButton(
                onPressed: validateForm,
                color: Color.fromRGBO(68, 192, 192, 1),
                child: new Text(
                  'СОХРАНИТЬ',
                  style: TextStyle(fontSize: 18.0),
                ))));
  }

  validateForm() async {
    Map validateResult;
    masterAllowed = true;
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      clientRecord.recordDate = new DateTime(
        newRecordDate.year,
        newRecordDate.month,
        newRecordDate.day,
        newRecordTime.hour,
        newRecordTime.minute,
      );
      validateResult = await dbHelper.updateClientRecordDateTime(clientRecord);
      if (validateResult['result'] == 'error') {
        masterAllowed = false;
        _formKey.currentState.validate();
      } else {
        Navigator.pop(context, 'updateState');
      }
    }
  }

  final _formKey = GlobalKey<FormState>();

  Widget buildForm(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(child: _recordDateField(context)),
                    Padding(padding: EdgeInsets.only(left: 20)),
                    Expanded(child: _recordTimeField(context)),
                  ],
                ),
              ),
              commonFormPadding(),
              _clientField(),
              commonFormPadding(),
              _masterField(),
              commonFormPadding(),
              _servicesField(),
              commonFormPadding(),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Expanded(child: _recordNewDateField(context)),
                    Padding(padding: EdgeInsets.only(left: 20)),
                    Expanded(child: _recordNewTimeField(context)),
                  ],
                ),
              ),
              commonFormPadding(),
              _paymentStatusField(),
              commonFormPadding(),
              RaisedButton(
                color: Color.fromRGBO(252, 245, 184, 1),               
                  child: Text('Добавить оплату', style: TextStyle( fontSize: 16)),
                  onPressed: _paymentBtnEnabled == true ? addPayment : null),
            ],
          ),
        ));
  }

  Widget _clientField() {
    String clientName = clientRecord != null ? clientRecord.clientName : '';
    return Row(
      children: [
        Text('Клиент: ', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
        Text(clientName, style: TextStyle( fontSize: 16)),
      ],
    );
  }

  Widget _masterField() {
    String masterName = clientRecord != null ? clientRecord.masterName : '';
    return Row(
      children: [Text('Мастер: ', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)), 
      Text(masterName, style: TextStyle( fontSize: 16))],
    );
  }

  Widget _servicesField() {
    String services = clientRecord != null ? clientRecord.serviceName : '';
    List<String> serviceList = services.split(', ');
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text('Услуги: ', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children:
              serviceList.map((serviceText) => Text(serviceText, style: TextStyle( fontSize: 16))).toList(),
        ),
        // Text(services),
      ],
    );
  }

  Widget _recordDateField(context) {
    DateTime date =
        clientRecord != null ? clientRecord.recordDate : DateTime.now();
    var controller = TextEditingController(text: formatDateOutput(date));
    return Column(children: <Widget>[
      Text('Дата записи', style: TextStyle(fontWeight: FontWeight.bold)),
      Text(controller.text),
    ]);
  }

  Widget _recordTimeField(BuildContext context) {
    DateTime date =
        clientRecord != null ? clientRecord.recordDate : DateTime.now();
    var timeController = TextEditingController(text: formatTimeOutput(date));
    Widget upperTextWidget =
        Text('Время записи:', style: TextStyle(fontWeight: FontWeight.bold));
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          upperTextWidget,
          Text(timeController.text),
        ]);
  }

  Widget _recordNewDateField(context) {
    final format = DateFormat("dd-MM-yyyy");
    var controller =
        TextEditingController(text: formatDateOutput(DateTime.now()));
    return Column(children: <Widget>[
      Text('Дата новой записи', style: TextStyle(fontWeight: FontWeight.bold)),
      DateTimeField(
        format: format,
        controller: controller,
        initialValue: DateTime.now(),
        onShowPicker: (context, currentValue) {
          return showDatePicker(
              context: context,
              firstDate: DateTime(2020),
              lastDate: DateTime(2100),
              initialDate: DateTime.now(),
              helpText: 'Выбери дату записи');
        },
        validator: (DateTime value) {
          if (value == null) {
            return ("Выберите дату записи");
          }
          if (value.isBefore(DateTime.now())) {
            return 'Нельзя записывать в прошлом';
          }
          if (!masterAllowed) {
            return 'Мастер занят';
          }
          return null;
        },
        onSaved: (DateTime date) {
          newRecordDate = date;
        },
      ),
    ]);
  }

  Widget _paymentStatusField() {
    return Row(
      children: [
        Text('Статус оплаты: ', style: TextStyle(fontWeight: FontWeight.bold)),
        Text(
          _paymentStatus,
          style: _paymentStatus == ClientRecord.paidStatus
              ? TextStyle(fontWeight: FontWeight.bold, color: Colors.green)
              : TextStyle(fontWeight: FontWeight.bold, color: Colors.red[300]),
        ),
      ],
    );
  }

  addPayment() async {
    Map paymentResult = await Navigator.push(
        _scaffoldKey.currentContext,
        MaterialPageRoute(
            builder: (context) => new AddPaymentTab(record: clientRecord)));
    if (paymentResult != null && paymentResult['status'] == 'success') {
      paymentList.add(paymentResult['newPayment']);
      setState(() {
        _paymentStatus = ClientRecord.paidStatus;
        _paymentBtnEnabled = false;
      });
    }
  }

  Widget _recordNewTimeField(BuildContext context) {
    var timeController =
        TextEditingController(text: formatTimeOutput(newRecordTime));
    Widget upperTextWidget = Text('Время новой записи:',
        style: TextStyle(fontWeight: FontWeight.bold));
    return popupTimePickerField(
      context,
      timeController,
      (DateTime time) {
        setState(() {
          newRecordTime = time;
          timeController.text = formatTimeOutput(time);
        });
      },
      upperTextWidget,
      'Сохранить',
      initialTime: newRecordTime,
    );
  }
}
