import 'package:flutter/material.dart';

import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/drawer.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/finance.dart';
import 'package:flutter_crm_app/screens/finance/consumptionTab.dart';

class FinanceListTab extends StatefulWidget {
  static const title = 'Финансы';
  static const androidIcon = Icon(Icons.attach_money);

  @override
  FinanceListState createState() => FinanceListState();
}

class FinanceListState extends State<FinanceListTab> {
  final _scaffoldMainKey = GlobalKey<ScaffoldState>();
  int summaStart = 0;
  int summaEnd = 0;
  List<Finance> financeList = [];
  DatabaseHelper dbHelper = DatabaseHelper.instance;

  Future<List<Finance>> _loadFinanceFromDB() async {
    financeList = await dbHelper.getFinanceList();
    summaEnd = 0;
    financeList.forEach((element) {
      if (element.financeType == Finance.income) {
        summaEnd += element.money;
      } else {
        summaEnd -= element.money;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldMainKey,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(FinanceListTab.title),
        actions: <Widget>[],
      ),
      drawer: AndroidDrawer(),
      body: FutureBuilder(
          future: _loadFinanceFromDB(),
          builder: (context, snapshot) {
            financeList = financeList ?? [];
            return buildBody();
          }),
      bottomNavigationBar: Container(
        height: 50,
        margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
        child: RaisedButton(
          onPressed: () {
            _navigateToCreate(context);
          },
          color: Color.fromRGBO(68, 192, 192, 1),
          child: new Text(
            'Добавить расход',
            style: TextStyle(fontSize: 18.0),
          ),
        ),
      ),
    );
  }

  _navigateToCreate(BuildContext context) async {
    String state = await Navigator.push(_scaffoldMainKey.currentContext,
        MaterialPageRoute(builder: (context) => new ConsumptionTab()));
    if (state == 'updateState') {
      setState(() {});
    }
  }

  Widget buildBody() {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: new SizedBox(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  financeDay(),
                  listView(),
                ]),
          )),
    );
  }

  Widget financeDay() {
    return Container(
      padding: EdgeInsets.all(20),
      child: Row(children: [
        Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text('Баланс:',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
          Padding(padding: EdgeInsets.only(right: 15)),
          Text(formatMoney(summaEnd),
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  color: Color.fromRGBO(68, 192, 192, 1))),
        ]),
      ]),
    );
  }

  Widget listView() {
    return DataTable(
      columns: const <DataColumn>[
        DataColumn(
          label: Text(
            'Сумма',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'Статья движения',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'Дата',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
      ],
      rows: financeList
          .map(
            (Finance finance) => DataRow(
              cells: <DataCell>[
                DataCell(
                  Container(
                    child: listMoneySell(finance),
                    width: 90,
                  ),
                ),
                DataCell(
                  listSell(finance.comment),
                ),
                DataCell(
                  listSell(formatDateTime(finance.dateCreate)),
                ),
              ],
            ),
          )
          .toList(),
    );
  }

  Widget listMoneySell(Finance finance) {
    TextStyle style = finance.financeType == Finance.income
        ? TextStyle(color: Colors.green)
        : TextStyle(color: Colors.red);
    Icon icon = finance.financeType == Finance.income
        ? Icon(Icons.keyboard_arrow_up, color: Colors.green)
        : Icon(Icons.keyboard_arrow_down, color: Colors.red);
    return Container(
      child: Row(children: [
        icon,
        Text(formatMoney(finance.money), style: style),
      ]),
    );
  }

  Widget listSell(String text) {
    return Container(
      child: Text(text),
    );
  }
}
