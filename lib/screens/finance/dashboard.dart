import 'package:flutter/material.dart';

import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/drawer.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/finance.dart';
import 'package:flutter_crm_app/screens/finance/consumptionTab.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class DashboardTab extends StatefulWidget {
  static const title = 'Dashboard';
  static const androidIcon = Icon(Icons.attach_money);

  @override
  DashboardState createState() => DashboardState();
}

class DashboardState extends State<DashboardTab> {
  final _scaffoldMainKey = GlobalKey<ScaffoldState>();
  int summaStart = 0;
  int summaEnd = 0;
  int totalIncome = 0;
  int totalSales = 0;
  int totalServices = 0;
  DatabaseHelper dbHelper = DatabaseHelper.instance;
  static charts.Color salesServiceColor =
      charts.MaterialPalette.green.makeShades(30)[5];
  static charts.Color salesSalesColor =
      charts.MaterialPalette.yellow.makeShades(30)[15];

  Future<int> _loadFinanceFromDB() async {
    Map financeMap = await dbHelper.getFinanceData();
    setState(() {
      totalIncome = financeMap['totalIncome'];
      totalSales = financeMap['totalSales'];
      totalServices = financeMap['totalServices'];
    });
    // summaEnd = 0;
    // financeList.forEach((element) {
    //   if (element.financeType == Finance.income) {
    //     summaEnd += element.money;
    //   } else {
    //     summaEnd -= element.money;
    //   }
    // });
    return 1;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadFinanceFromDB();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldMainKey,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(DashboardTab.title),
        actions: <Widget>[],
      ),
      drawer: AndroidDrawer(),
      body: buildBody(),
    );
  }

  Widget buildBody() {
    return Column(
      children: <Widget>[
        // _buildCalendarRangeBlock(),
        _buildSalesBlock(),
        // _buildRecordBlock()
      ],
    );
  }

  Widget _buildCalendarRangeBlock() {
    return Text('КАЛЕНДАРИК');
  }

  Widget _buildSalesBlock() {
    return styledContainer(
      content: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            children: [
              Text(
                'ПРОДАЖИ',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26),
              ),
              commonFormPadding(padding: 5),
              Column(
                //  crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Объем продаж:', style: TextStyle(fontSize: 16)),
                  Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                    Text(
                      totalIncome.toString(),
                      style: TextStyle(fontSize: 22),
                    ),
                    // Text(
                    //   '+100%',
                    //   style: TextStyle(color: Colors.green),
                    // )
                  ]),
                ],
              ),
              commonFormPadding(padding: 10),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      border: Border(
                          left:
                              BorderSide(width: 10, color: Colors.green[200])),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                totalServices.toString(),
                                style: TextStyle(fontSize: 22),
                              ),
                              // Text(
                              //   '+100%',
                              //   style: TextStyle(color: Colors.green),
                              // )
                            ]),
                        Text('Услуги'),
                      ],
                    ),
                  ),
                  commonFormPadding(padding: 4),
                  Container(
                    decoration: BoxDecoration(
                      border: Border(
                          left:
                              BorderSide(width: 10, color: Colors.yellow[200])),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            // mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                totalSales.toString(),
                                style: TextStyle(fontSize: 22),
                              ),
                              // Text(
                              //   '+100%',
                              //   style: TextStyle(color: Colors.green),
                              // )
                            ]),
                        Text('Продажи'),
                      ],
                    ),
                  ),
                  // _buildPie(),
                ],
              ),
            ],
          ),
          Container(width: 200, height: 200, child: _buildPie())
        ],
      ),
    );
  }

  Widget _buildRecordBlock() {
    return Text('Записи');
  }

  Widget _buildPie() {
    return SizedBox(
        width: 200,
        child: charts.PieChart(
          _createSampleData(totalSales, totalServices),
            animate: true,
            defaultRenderer: charts.ArcRendererConfig(arcWidth: 40)));
  }

  static List<charts.Series<PieLinear, int>> _createSampleData(totalSales, totalServices) {
    final data = [
      new PieLinear(0, totalServices, salesServiceColor),
      new PieLinear(1, totalSales, salesSalesColor),
    ];

    return [
      new charts.Series<PieLinear, int>(
        id: 'Sales',
        domainFn: (PieLinear sales, _) => sales.financeType,
        measureFn: (PieLinear sales, _) => sales.financeAmount,
        colorFn: (PieLinear sales, _) => sales.color,
        data: data,
      )
    ];
  }

  Widget styledContainer({Widget content, EdgeInsets customPadding}) {
    final padding = customPadding ?? EdgeInsets.all(5);
    return Container(
        padding: padding,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: content);
  }
}

class PieLinear {
  final int financeType;
  final int financeAmount;
  final charts.Color color;

  PieLinear(this.financeType, this.financeAmount, this.color);
}
