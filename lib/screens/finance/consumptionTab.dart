import 'package:flutter/material.dart';

import 'package:flutter_crm_app/drawer.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/models/consumption.dart';
import 'package:flutter_crm_app/models/employee.dart';
import 'package:flutter_crm_app/models/finance.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class ConsumptionTab extends StatefulWidget {
  static const title = 'Расход';
  static const androidIcon = Icon(Icons.attach_money);

  @override
  ConsumptionState createState() => ConsumptionState();
}

class ConsumptionState extends State<ConsumptionTab> {
  static const String salaryTextConst = 'Зарплата сотрудника:';
  static const String consumptionTextConst = 'Величина расхода:';
  final _scaffoldMainKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  DatabaseHelper dbHelper = DatabaseHelper.instance;
  Employee selectedMaster;
  int consumptionSelected = 1;
  Widget switchedWidget;
  ConsumptionType consumptionType;
  int consumptionValue = 0;
  String consumptionTypeLabel = salaryTextConst;
  bool masterSelected = true;

  @override
  void initState() {
    switchedWidget = _salaryForm();
    consumptionTypeLabel = salaryTextConst;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldMainKey,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(ConsumptionTab.title),
        actions: <Widget>[],
      ),
      body: buildBody(),
      bottomNavigationBar: Container(
        height: 50,
        margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
        child: RaisedButton(
          onPressed: validateAndSendForm,
          color: Color.fromRGBO(68, 192, 192, 1),
          child: new Text(
            'Сохранить',
            style: TextStyle(fontSize: 18.0),
          ),
        ),
      ),
    );
  }

  validateAndSendForm() async {
    bool valid = true;
    String comment;
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      masterSelected = true;
      if (consumptionSelected == 1) {
        if (selectedMaster == null) {
          masterSelected = false;
          valid = false;
          _formKey.currentState.validate();
        } else {
          comment = 'Выдача ЗП сотруднику: ' + selectedMaster.name;
        }
      } else {
        comment = consumptionType.name;
      }
    }
    if (valid) {
      dbHelper.addConsumption(comment, consumptionValue);
      Navigator.pop(context, 'updateState');
    }
  }

  Widget buildBody() {
    return Container(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _selectConsumptionType(),
              commonFormPadding(),
              _animatedSwitch(),
              commonFormPadding(),
              _consumptionAmountField(),
            ],
          ),
        ));
  }

  Widget _selectConsumptionType() {
    return Row(
      children: <Widget>[
        Row(children: <Widget>[
          const Text('Расчёт ЗП'),
          Radio(
            activeColor: Color.fromRGBO(68, 192, 192, 1),
            value: 1,
            groupValue: consumptionSelected,
            onChanged: (int value) {
              setState(() {
                consumptionSelected = value;
                switchedWidget = _salaryForm();
                consumptionTypeLabel = salaryTextConst;
                consumptionValue = 0;
              });
            },
          ),
        ]),
        Row(children: <Widget>[
          const Text('Расход'),
          Radio(
            activeColor: Color.fromRGBO(68, 192, 192, 1),
            value: 2,
            groupValue: consumptionSelected,
            onChanged: (int value) {
              setState(() {
                consumptionSelected = value;
                switchedWidget = _consumptionForm();
                consumptionTypeLabel = consumptionTextConst;
                consumptionValue = 0;
              });
            },
          ),
        ]),
      ],
    );
  }

  Widget _animatedSwitch() {
    return AnimatedSwitcher(
      duration: const Duration(seconds: 0),
      child: switchedWidget,
    );
  }

  Widget _consumptionForm() {
    return Column(
      children: <Widget>[
        _consumptionTypeDropdown(),
        // _consumptionAmountField(),
      ],
    );
  }

  Widget _consumptionTypeDropdown() {
    var callback = (ConsumptionType newValue) {
      setState(() {
        consumptionType = newValue;
      });
    };
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Вид расхода:', style: TextStyle(fontWeight: FontWeight.bold)),
        searchPopupField(dbHelper.consumptionTypeList(), 'Выберите вид расхода', 'text2',
            consumptionType, callback, null),
      ],
    );
  }

  Widget _consumptionAmountField() {
    var moneyController = MoneyMaskedTextController(
        rightSymbol: ' ₽',
        initialValue: consumptionValue.toDouble(),
        thousandSeparator: ' ',
        decimalSeparator: '',
        precision: 0);
    return Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
          Text(consumptionTypeLabel ?? salaryTextConst,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0)),
          TextFormField(
              controller: moneyController,
              decoration: const InputDecoration(
                hintText: '',
              ),
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Поле обязательно к заполнению';
                }
                if (moneyController.numberValue.toInt() <= 0) {
                  return 'Введите значение';
                }
                return null;
              },
              onSaved: (String value) {
                consumptionValue = moneyController.numberValue.toInt();
              })
        ]));
  }

  Widget _salaryForm() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _masterField(),
        commonFormPadding(),
        _calculateSalary(),
      ],
    );
  }

  Widget _masterField() {
    var callback = (value) {
      setState(() {
        selectedMaster = value;
      });
    };
    var validator = (value) {
      if (value == null) {
        return "";
      }
      if (!masterSelected) {
        return '';
      }
      return null;
    };
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Сотрудник',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        searchPopupField(dbHelper.employeeList(), "Выберите сотрудника", "",
            selectedMaster, callback, validator),
      ],
    );
  }

  Widget _calculateSalary() {
    return Center(
        child: RaisedButton(
            color: Color.fromRGBO(252, 245, 184, 1),
            child: new Text(
              'Рассчитать зарплату',
            ),
            onPressed: _calculateFunction));
  }

  void _calculateFunction() async {
    DateTime dateFrom = DateTime(2020, 01, 01);
    DateTime dateTo = DateTime(2020, 12, 12);
    int bonusSalaryValue =
        await dbHelper.calculateSalary(selectedMaster, dateFrom, dateTo);

    setState(() {
      switchedWidget = _salaryForm();
      consumptionValue = selectedMaster.salaryValue + bonusSalaryValue;
    });
  }

  Widget _salaryAmountField() {
    return Container(
      child: Row(
        children: <Widget>[
          Text('Зарплата сотрудника:',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0)),
          Padding(padding: EdgeInsets.only(right: 10)),
          Text(
            consumptionValue == 0
                ? formatMoney(0)
                : formatMoney(consumptionValue),
            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green),
          ),
        ],
      ),
    );
  }
}
