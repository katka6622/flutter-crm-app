// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter_crm_app/models/employeeWorkDay.dart';

import 'package:flutter_date_pickers/flutter_date_pickers.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/employee.dart';
import 'dart:ui';

const hourHeight = 80.0;
const int workingHours = 17;
const int gridHeigth = 90;
const int paddingFault = 100;

class EmployeeWorkCalendarTab extends StatefulWidget {
  static const title = 'График работы';
  static const androidIcon = Icon(Icons.insert_invitation);
  final Employee employee;

  const EmployeeWorkCalendarTab({Key key, this.employee}) : super(key: key);

  @override
  WorkCalendarState createState() => WorkCalendarState();
}

class WorkCalendarState extends State<EmployeeWorkCalendarTab>
    with TickerProviderStateMixin {
  final _scaffoldMainKey = GlobalKey<ScaffoldState>();
  DatabaseHelper dbHelper = DatabaseHelper.instance;
  final DateTime startDate = DateTime(2020, 06, 22);
  final DateTime lastDate = DateTime(2020, 06, 28);
  DatePeriod _selectedPeriod;
  DateTime selectedDay;
  DateTime selectDateFrom;
  DateTime selectDateTo;
  List<EmployeeWorkDay> daysList = [];
  int isWorkDays = 1;

  @override
  void initState() {
    super.initState();
    selectDateFrom = DateTime.now();
    selectDateTo = DateTime.now();
    _selectedPeriod = new DatePeriod(selectDateFrom, selectDateTo);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldMainKey,
        appBar: AppBar(
          title: Text(EmployeeWorkCalendarTab.title),
          actions: <Widget>[],
        ),
        body: FutureBuilder(
          future: dbHelper.getEmployeeWorkDays(widget.employee),
          builder: (context, snapshot) {
            daysList = snapshot.data ?? [];
            return Column(children: [
              buildWorkCalendar(daysList),
              radioButton(),
            ]);
          },
        ),
        bottomNavigationBar: Container(
            height: 50,
            margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
            child: RaisedButton(
                onPressed: () => {
                      dbHelper.addEmployeeWorkDay(_selectedPeriod, isWorkDays, widget.employee),
                      setState(() => {}),
                    },
                color: Color.fromRGBO(68, 192, 192, 1),
                child: new Text(
                  'СОХРАНИТЬ',
                  style: TextStyle(fontSize: 18.0),
                ))));
  }

  Widget buildWorkCalendar(List<EmployeeWorkDay> daysList) {
    return SizedBox(
        width: 430,
        child: RangePicker(
          firstDate: DateTime(2020, 01, 01),
          lastDate: DateTime(2040, 12, 12),
          selectedPeriod: _selectedPeriod,
          eventDecorationBuilder: _eventDecorationBuilder,
          onChanged: (DatePeriod datePeriod) => {
            _onCalendarChange(datePeriod),
          },
        ));
  }

  Widget radioButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Row(children: <Widget>[
          const Text('Рабочий день'),
          Radio(
            activeColor: Color.fromRGBO(68, 192, 192, 1),
            value: 1,
            groupValue: isWorkDays,
            onChanged: (int value) {
              setState(() {
                isWorkDays = value;
              });
            },
          ),
        ]),
        Row(children: <Widget>[
          const Text('Нерабочий день'),
          Radio(
            activeColor: Color.fromRGBO(68, 192, 192, 1),
            value: 0,
            groupValue: isWorkDays,
            onChanged: (int value) {
              setState(() {
                isWorkDays = value;
              });
            },
          ),
        ]),
      ],
    );
  }

  BoxDecoration _workDayDecoration() {
    return BoxDecoration(
        border: Border.all(
          color: Colors.green,
        ),
        borderRadius: BorderRadius.all(Radius.circular(3.0))
    );
  }

  BoxDecoration _nonWorkDayDecoration() {
    return BoxDecoration(
        border: Border.all(
          color: Colors.red,
        ),
        borderRadius: BorderRadius.all(Radius.circular(3.0))
    );
  }

  EventDecoration _eventDecorationBuilder(DateTime date) {
    List<DateTime> workDays = [];
    List<DateTime> nonWorkDays = [];
    daysList.forEach((element) {
      if (element.isWorkDay == 1) {
        workDays.add(element.date);
      } else {
        nonWorkDays.add(element.date);
      }
    });

    bool isWorkDayBool = workDays?.any((DateTime d) => date.year == d.year && date.month == d.month && d.day == date.day) ?? false;
    if (isWorkDayBool) {
      return EventDecoration(boxDecoration: _workDayDecoration());
    }

    bool isNonWorkDayBool = nonWorkDays?.any((DateTime d) => date.year == d.year && date.month == d.month && d.day == date.day) ?? false;
    if (isNonWorkDayBool) {
      return EventDecoration(boxDecoration: _nonWorkDayDecoration());
    }
    return null;
  }

  _onCalendarChange(DatePeriod newPeriod) {
    setState(() {
      _selectedPeriod = newPeriod;
    });
  }
}
