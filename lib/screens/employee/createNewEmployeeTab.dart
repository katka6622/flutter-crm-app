// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:intl/intl.dart';

import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/employee.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class CreateNewEmployeeTab extends StatefulWidget {
  static const title = 'Добавление сотрудника';
  static const androidIcon = Icon(Icons.add);

  @override
  NewEmployeeState createState() => NewEmployeeState();
}

class NewEmployeeState extends State<CreateNewEmployeeTab> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  Employee newEmployee = Employee();
  String roleValue = ConstantValues.roleList[0];
  String genderValue = ConstantValues.gengerList[0];

  @override
  Widget build(BuildContext context) {
    DatabaseHelper dbHelper = DatabaseHelper.instance;
    return Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          title: Text(CreateNewEmployeeTab.title),
        ),
        body: buildForm(context),
        bottomNavigationBar: Container(
            height: 50,
            margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
            child: RaisedButton(
                onPressed: () => {
                      if (_formKey.currentState.validate())
                        {
                          _formKey.currentState.save(),
                          dbHelper.insertEmployee(newEmployee),
                          Navigator.pop(context, 'updateState'),
                        }
                    },
                color: Color.fromRGBO(68, 192, 192, 1),
                child: new Text(
                  'СОЗДАТЬ',
                  style: TextStyle(fontSize: 18.0),
                ))));
  }

  final _formKey = GlobalKey<FormState>();

  Widget buildForm(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _nameField(),
              commonFormPadding(),
              _phoneField(),
              commonFormPadding(),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                _roleField(),
                _genderField(),
              ]),
              commonFormPadding(),
              _specialityField(),
              commonFormPadding(),
              _birthdayField(),
              commonFormPadding(),
              _salaryValueField(),
              commonFormPadding(),
              _salaryPercentField(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _nameField() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Имя:', style: TextStyle(fontWeight: FontWeight.bold)),
      TextFormField(
          decoration: const InputDecoration(
            hintText: 'Введите имя',
          ),
          validator: (value) {
            if (value.isEmpty) {
              return 'Поле обязательно к заполнению';
            }
            return null;
          },
          onSaved: (String value) {
            newEmployee.name = value;
          }),
    ]);
  }

  Widget _phoneField() {
    return phoneFormField('', (String value) {
      newEmployee.phone = value;
    });
  }

  Widget _roleField() {
    var callback = (String newValue) {
      setState(() {
        roleValue = newValue;
        newEmployee.role = newValue;
      });
    };
    newEmployee.role = roleValue;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Должность:', style: TextStyle(fontWeight: FontWeight.bold)),
        dropdownField(ConstantValues.roleList, callback, roleValue),
      ],
    );
  }

  Widget _genderField() {
    var callback = (String newValue) {
      setState(() {
        genderValue = newValue;
        newEmployee.gender = newValue;
      });
    };
    newEmployee.gender = genderValue;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Пол:', style: TextStyle(fontWeight: FontWeight.bold)),
        dropdownField(ConstantValues.gengerList, callback, genderValue),
      ],
    );
  }

  Widget _specialityField() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Специализация:', style: TextStyle(fontWeight: FontWeight.bold)),
      TextFormField(
          decoration: const InputDecoration(
            hintText: 'Введите Специализацию',
          ),
          onSaved: (String value) {
            newEmployee.speciality = value;
          }),
    ]);
  }

  Widget _birthdayField() {
    final format = DateFormat("dd-MM-yyyy");
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('День рождения', style: TextStyle(fontWeight: FontWeight.bold)),
          DateTimeField(
            format: format,
            onShowPicker: (context, currentValue) {
              return showDatePicker(
                  context: context,
                  firstDate: DateTime(1900),
                  initialDate: currentValue ?? DateTime.now(),
                  lastDate: DateTime(2100));
            },
            onSaved: (DateTime birthday) {
              newEmployee.birthday = birthday;
            },
          ),
        ]);
  }

  Widget _salaryValueField() {
    var _moneyController = MoneyMaskedTextController(
        rightSymbol: ' ₽',
        initialValue: 0,
        thousandSeparator: ' ',
        decimalSeparator: '',
        precision: 0);
    return Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
          Text('Фиксированый оклад:',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0)),
          TextFormField(
              controller: _moneyController,
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Поле обязательно к заполнению';
                }
                if (_moneyController.numberValue.toInt() < 0) {
                  return 'Не стоит меньше нуля делать';
                }
                return null;
              },
              onSaved: (String value) {
                newEmployee.salaryValue =
                    _moneyController.numberValue.toInt();
              })
        ]));
  }

  Widget _salaryPercentField() {
    var _moneyController = MoneyMaskedTextController(
        rightSymbol: '%',
        thousandSeparator: '',
        decimalSeparator: '',
        precision: 0);
    return Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
          Text('Процент с услуг',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0)),
          TextFormField(
              controller: _moneyController,
              keyboardType: TextInputType.number,
              // inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)]
              validator: (value) {
                if (value.isEmpty) {
                  return 'Поле обязательно к заполнению';
                }
                if (_moneyController.numberValue.toDouble() < 0) {
                  return 'Не стоит меньше нуля делать';
                }
                if (_moneyController.numberValue.toDouble() > 100.0) {
                  return 'Многовато процентов';
                }
                return null;
              },
              onSaved: (String value) {
                newEmployee.salaryPercent = _moneyController.numberValue.toDouble();
              })
        ]));
  }
}
