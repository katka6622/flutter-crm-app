// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';

import 'package:flutter_crm_app/drawer.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/employee.dart';
import 'package:flutter_crm_app/screens/employee/employeeWorkCalendarTab.dart';
import 'dart:ui';

const hourHeight = 80.0;
const int workingHours = 17;
const int gridHeigth = 90;
const int paddingFault = 100;

class EmployeeWorkingDaysTab extends StatefulWidget {
  static const title = 'График работы';
  static const androidIcon = Icon(Icons.insert_invitation);

  @override
  WorkingDayState createState() => WorkingDayState();
}

class WorkingDayState extends State<EmployeeWorkingDaysTab>
    with TickerProviderStateMixin {
  final _scaffoldMainKey = GlobalKey<ScaffoldState>();
  DatabaseHelper dbHelper = DatabaseHelper.instance;
   Future<List<Employee>> futureEmployeeList;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    futureEmployeeList = _loadData();
  }

  Future<List<Employee>> _loadData() async {
    return await dbHelper.employeeList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldMainKey,
      appBar: AppBar(
        title: Text(EmployeeWorkingDaysTab.title),
        actions: <Widget>[],
      ),
      drawer: AndroidDrawer(),
      body: FutureBuilder(
        future: futureEmployeeList,
        builder: (context, snapshot) {
          List<Employee> employeeList = snapshot.data ?? [];
          return buildEmployeeList(employeeList);
        },
      ),
    );
  }

  Widget buildEmployeeList(List<Employee> employeeList) {
    return ListView.builder(
        padding: EdgeInsets.all(0),
        itemCount: employeeList.length,
        itemBuilder: (context, index) {
          Employee employee = employeeList[index];
          return _buildEmployeeRow(employee);
        });
  }

  Widget _buildEmployeeRow(Employee employee) {
    return Container(
        decoration: BoxDecoration(
            // border: Border.all(color: Colors.grey[300]),
            border: Border.all(color: Colors.grey[200]),
            color: Colors.grey[50]),
        width: double.infinity,
        height: 100,
        padding: EdgeInsets.only(left: 20.0, bottom: 10, top: 10),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(
                employee.name,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
              ),
              RaisedButton(
                color: Color.fromRGBO(68, 192, 192, 0.8),
                  child: Text('Посмотреть рабочие дни'),
                  onPressed: () => {
                        _moveToEmployeeWorkingDays(employee),
                      })
            ]));
  }

  _moveToEmployeeWorkingDays(Employee employee) async {
    await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => EmployeeWorkCalendarTab(employee: employee)),
    );
  }
}
