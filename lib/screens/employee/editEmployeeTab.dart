// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/employee.dart';

class EditEmployeeTab extends StatefulWidget {
  static const title = 'Сотрудник';
  static const androidIcon = Icon(Icons.list);
  final Employee employee;

  const EditEmployeeTab({Key key, this.employee}) : super(key: key);

  @override
  EditEmployeeState createState() => EditEmployeeState();
}

class EditEmployeeState extends State<EditEmployeeTab> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String roleValue = ConstantValues.roleList[0];
  DatabaseHelper dbHelper = DatabaseHelper.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          title: Text(EditEmployeeTab.title),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.delete_outline,
                color: Colors.red[400],
              ),
              onPressed: () => {
                _formKey.currentState.save(),
                dbHelper.removeEmployee(widget.employee.id),
                Navigator.pop(context, 'updateState'),
              },
            ),
          ],
        ),
        body: buildForm(context),
        bottomNavigationBar: Container(
            height: 50,
            margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
            child: RaisedButton(
                onPressed: () => {
                      if (_formKey.currentState.validate())
                        {
                          _formKey.currentState.save(),
                          dbHelper.updateEmployee(widget.employee),
                          Navigator.pop(context, 'updateState'),
                        }
                    },
                color: Color.fromRGBO(68, 192, 192, 1),
                child: new Text(
                  'СОХРАНИТЬ',
                  style: TextStyle(fontSize: 18.0),
                ))));
  }

  final _formKey = GlobalKey<FormState>();

  Widget buildForm(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _nameField(),
              commonFormPadding(),
              _phoneField(),
              commonFormPadding(),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                _roleField(),
                _genderField(),
              ]),
              commonFormPadding(),
              _specialityField(),
              commonFormPadding(),
              _birthdayField(),
              commonFormPadding(),
              _salaryValueField(),
              commonFormPadding(),
              _salaryPercentField(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _nameField() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Имя:', style: TextStyle(fontWeight: FontWeight.bold)),
      TextFormField(
          initialValue: widget.employee.name,
          decoration: const InputDecoration(
            hintText: 'Имя сотрудника',
          ),
          validator: (value) {
            if (value.isEmpty) {
              return 'Поле обязательно к заполнению';
            }
            return null;
          },
          onSaved: (String value) {
            widget.employee.name = value;
          }),
    ]);
  }

  Widget _phoneField() {
    return phoneFormField(widget.employee.phone, (String value) {
      widget.employee.phone = value;
    });
  }

  Widget _roleField() {
    var callback = (String newValue) {
      setState(() {
        widget.employee.role = newValue;
      });
    };

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Должность:', style: TextStyle(fontWeight: FontWeight.bold)),
        dropdownField(ConstantValues.roleList, callback, widget.employee.role),
      ],
    );
  }

  Widget _genderField() {
    var callback = (String newValue) {
      setState(() {
        widget.employee.gender = newValue;
      });
    };

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Пол:', style: TextStyle(fontWeight: FontWeight.bold)),
        dropdownField(
            ConstantValues.gengerList, callback, widget.employee.gender),
      ],
    );
  }

  Widget _specialityField() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Специализация:', style: TextStyle(fontWeight: FontWeight.bold)),
      TextFormField(
          initialValue: widget.employee.speciality,
          decoration: const InputDecoration(
            hintText: 'Специализация',
          ),
          onSaved: (String value) {
            widget.employee.speciality = value;
          }),
    ]);
  }

  Widget _birthdayField() {
    final format = DateFormat("dd-MM-yyyy");
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('День рождения', style: TextStyle(fontWeight: FontWeight.bold)),
          DateTimeField(
            initialValue: widget.employee.birthday,
            format: format,
            onShowPicker: (context, currentValue) {
              return showDatePicker(
                context: context,
                firstDate: DateTime(1900),
                initialDate: widget.employee.birthday ?? DateTime.now(),
                lastDate: DateTime(2100),
              );
            },
            onSaved: (DateTime birthday) {
              widget.employee.birthday = birthday;
            },
          ),
        ]);
  }

  Widget _salaryValueField() {
    var _moneyController = MoneyMaskedTextController(
        rightSymbol: ' ₽',
        initialValue: widget.employee.salaryValue.toDouble(),
        thousandSeparator: ' ',
        decimalSeparator: '',
        precision: 0);
    return Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
          Text('Фиксированый оклад:',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0)),
          TextFormField(
              controller: _moneyController,
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Поле обязательно к заполнению';
                }
                if (_moneyController.numberValue.toInt() < 0) {
                  return 'Не стоит меньше нуля делать';
                }
                return null;
              },
              onSaved: (String value) {
                widget.employee.salaryValue =
                    _moneyController.numberValue.toInt();
              })
        ]));
  }

  Widget _salaryPercentField() {
    var _moneyController = MoneyMaskedTextController(
        initialValue: widget.employee.salaryPercent.toDouble(),
        rightSymbol: '%',
        thousandSeparator: '',
        decimalSeparator: '',
        precision: 0);
    return Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
          Text('Процент с услуг',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0)),
          TextFormField(
              controller: _moneyController,
              keyboardType: TextInputType.number,
              // inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)]
              validator: (value) {
                if (value.isEmpty) {
                  return 'Поле обязательно к заполнению';
                }
                if (_moneyController.numberValue.toDouble() < 0) {
                  return 'Не стоит меньше нуля делать';
                }
                if (_moneyController.numberValue.toDouble() > 100.0) {
                  return 'Многовато процентов';
                }
                return null;
              },
              onSaved: (String value) {
                widget.employee.salaryPercent =
                    _moneyController.numberValue.toDouble();
              })
        ]));
  }
}
