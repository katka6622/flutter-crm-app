// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';

import 'package:flutter_crm_app/drawer.dart';
import 'package:flutter_crm_app/helpers/ui.dart';
import 'package:flutter_crm_app/helpers/database_helper.dart';
import 'package:flutter_crm_app/models/employee.dart';
import 'package:flutter_crm_app/screens/employee/createNewEmployeeTab.dart';
import 'package:flutter_crm_app/screens/employee/editEmployeeTab.dart';

class EmployeeListTab extends StatefulWidget {
  static const title = 'Сотрудники';
  static const androidIcon = Icon(Icons.assignment_ind);

  @override
  EmployeeListState createState() => EmployeeListState();
}

class EmployeeListState extends State<EmployeeListTab> {
  final _scaffoldMainKey = GlobalKey<ScaffoldState>();
  DatabaseHelper dbHelper = DatabaseHelper.instance;
  Future<List<Employee>> employeeList;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldMainKey,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(EmployeeListTab.title),
        actions: <Widget>[],
      ),
      drawer: AndroidDrawer(), //call
      body: buildUsersList(context),
      floatingActionButton: Theme(
        data: Theme.of(context).copyWith(
          colorScheme: Theme.of(context)
              .colorScheme
              .copyWith(secondary: Color.fromRGBO(68, 192, 192, 1)),
        ),
        child: FloatingActionButton(
          onPressed: () {
            _navigateToCreate(context, _scaffoldMainKey);  
          },
          child: Icon(Icons.add),
        ),
      ),
    );
  }

  _navigateToCreate(BuildContext context, _scaffoldMainKey) async {
    var createResult = await Navigator.push(_scaffoldMainKey.currentContext,
        MaterialPageRoute(builder: (context) => new CreateNewEmployeeTab()));

    if (createResult == 'updateState') {
      setState(() {});
    }
  }

  _navigateToEdit(BuildContext context, Employee employee) async {
    var updatedStatus = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => EditEmployeeTab(employee: employee)),
    );
    if (updatedStatus == 'updateState') {
      setState(() {});
    }
  }

  Future<List<Employee>> _loadData() async {
    return await dbHelper.employeeList();
  }

  Widget buildUsersList(BuildContext context) {
    return FutureBuilder<List<Employee>>(
        future: _loadData(),
        builder: (context, snapshot) {
          List<Employee> serviceList = snapshot.data ?? [];
          return ListView.builder(
              padding: const EdgeInsets.all(0),
              itemCount: serviceList.length,
              itemBuilder: (context, index) {
                Employee service = serviceList[index];
                return _buildRow(service);
              });
        });
  }

  Widget _buildRow(Employee employee) {
    return ListTile(
        title: _buildUserInfoRow(employee),
        onTap: () async {
          _navigateToEdit(context, employee);
        });
  }

  Widget _buildUserInfoRow(Employee employee) {
    return Container(
      decoration: BoxDecoration(
          // border: Border.all(color: Colors.grey[300]),
          border: Border(bottom: BorderSide(color: Colors.grey[200])),
          ),
      width: double.infinity,
      height: 80,
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(employee.name,
                  style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0)),
              Text(
                employee.role,
                style: TextStyle(fontSize: 11.0, color: Colors.black38),
              ),
              Text(
                employee.speciality,
                style: TextStyle(fontSize: 14.0),
              ),
            ]),
        Icon(Icons.more_vert, color: Color.fromRGBO(68, 192, 192, 1)),
      ]),
    );
  }
}
